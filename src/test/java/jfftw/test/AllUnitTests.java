/*
 * Version 1.1 19/12/2012
 * Reestructuraci�n de los tests y refinamiento de la salida por pantalla de
 * los mismos. 
 * 
 * Version 1.0 01/04/2012
 * 
 * � 2012 Jos� M. Garc�a Dom�nguez
 */
package jfftw.test;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;



/**
 * @author jmgarcia
 * @version 1.1
 */
@RunWith(Suite.class)

@SuiteClasses({
	Test02_FFT_2D.class,
	Test03_FFT_3D.class,
	Test04_MultithreadFFT.class
})

public class AllUnitTests {
	
	public String TEST_STR = getClass().getName() + "::";
	
}
