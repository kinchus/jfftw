/*
 * Version 1.0 25/04/2013
 * 
 *
 * 2013 � Jos� M. Garc�a Dom�nguez
 *
 */
package jfftw.test;

import static org.junit.Assert.*;
import jfftw.ArrayUtil;
import jfftw.FFTW;
import jfftw.FFTWComplex;
import jfftw.FFTWComplexEngine;
import jfftw.FFTWReal;

import org.junit.Test;


/**
 * @author jmgarcia
 * @version 1.0
 */
public class Test02_FFT_2D {

	private String TEST_STR = getClass().getName() + "::";
	
	@Test
	public final void testEngine() {
		FFTWComplexEngine ce = new FFTWComplexEngine(2048,FFTW.FORWARD,false);
		System.out.println(TEST_STR + "testEngine ");
		assertNotNull(ce);
	}
	
	/**
	 * Test method for {@link es.imgworks.classifier.datamanip.ImgArray#doFftR()}.
	 */
	@Test
	public void testTwoDimensionsDouble1() {
		
		System.out.println(TEST_STR + "testTwoDimensionsDouble1");

		double [] dInterm;
		double [] dResult;
		double [] dData = {
			//
			0, 1, 2, 3, 4, 
			5, 6 ,7, 8, 9,
			10, 11, 12, 13, 14,
			15, 16, 17, 18, 19, 
			20, 21, 22, 23, 24,
		};

		FFTWReal fftwReal = new FFTWReal();
		
		dInterm = fftwReal.twoDimensionalForward(5, 5, dData);
		dResult = fftwReal.twoDimensionalBackward(5, 5, dInterm);

		for (int i=0;i<dResult.length;i++) {
			assertEquals(dData[i], dResult[i]/25.0, 0.000001);
		}

		return;
	}

	/**
	 * Test method for {@link es.imgworks.classifier.datamanip.ImgArray#doFftR()}.
	 */
	@Test
	public void testTwoDimensionsFloat1() {
		
		System.out.println(TEST_STR + "testTwoDimensionsFloat1");

		float [] fInterm;
		float [] fResult;
		float [] fData = {
			//
			0, 1, 2, 3, 4, 
			5, 6 ,7, 8, 9,
			10, 11, 12, 13, 14,
			15, 16, 17, 18, 19, 
			20, 21, 22, 23, 24,
		};

		FFTWReal fftwReal = new FFTWReal();
		
		fInterm = fftwReal.twoDimensionalForward(5, 5, fData);
		fResult = fftwReal.twoDimensionalBackward(5, 5, fInterm);

		for (int i=0;i<fResult.length;i++) {
			assertEquals(fData[i], fResult[i]/25.0, 0.000001);
		}

		return;
	}

	/**
	 * Test method for {@link es.imgworks.classifier.datamanip.ImgArray#doFftR()}.
	 */
	@Test
	public void testTwoDimensionsDouble2() {

		System.out.println(TEST_STR + "testTwoDimensionsDouble2");

		double [] data = {
				0, 1, 2, 3, 4, 
				5, 6 ,7, 8, 9,
				10, 11, 12, 13, 14,
				15, 16, 17, 18, 19, 
				20, 21, 22, 23, 24
		};

		double []dest = {
				300.0, -12.5000,  -12.5000,  
				-62.5000, 0, 0,
				-62.5000, 0, 0,
				-62.5000, 0, 0,
				-62.5000, 0, 0
		};

		
		FFTWReal fftwReal = new FFTWReal();
		
		double [] interm = fftwReal.twoDimensionalForward(5,5, data);

		/*
		for (int i=0;i<dest.length;i++) {
			assertEquals(dest[i], result[i], 0.000001);
		}
		 */	

		double [] result = fftwReal.twoDimensionalBackward(5,5, interm);

		for (int i=0;i<dest.length;i++) {
			assertEquals(data[i], result[i]/25.0, 0.000001);
		}
		// Guarda los planes
		
	}
	

	/**
	 * Test method for {@link es.imgworks.classifier.datamanip.ImgArray#doFftR()}.
	 */
	@Test
	public void testTwoDimensionsFloat2() {

		System.out.println(TEST_STR + "testTwoDimensionsFloat2");

		float [] data = {
				0, 1, 2, 3, 4, 
				5, 6 ,7, 8, 9,
				10, 11, 12, 13, 14,
				15, 16, 17, 18, 19, 
				20, 21, 22, 23, 24
		};

		float []dest = {
				300.0F, -12.5000F,  -12.5000F,  
				-62.5000F, 0, 0,
				-62.5000F, 0, 0,
				-62.5000F, 0, 0,
				-62.5000F, 0, 0
		};

		
		FFTWReal fftwReal = new FFTWReal();
		
		float [] interm = fftwReal.twoDimensionalForward(5,5, data);

		/*
		for (int i=0;i<dest.length;i++) {
			assertEquals(dest[i], result[i], 0.000001);
		}
		 */	

		float [] result = fftwReal.twoDimensionalBackward(5,5, interm);

		for (int i=0;i<dest.length;i++) {
			assertEquals(data[i], result[i]/25.0, 0.000001);
		}
		// Guarda los planes
		
	}
	

	@Test
	public final void testTwoDimensionsDouble3() {
		
		System.out.println(TEST_STR + "testTwoDimensionsDouble3");
		
		int width = 200;                  // Size of array
		int height = 300;

		//             Make complex array (twice the size)
		double[] data = new double[2*width*height];

		//        Fill both parts with random using the ArrayUtil
		//        class to do the indexing.
		for(int i = 0; i < width*height; i++) {
		    ArrayUtil.setComplex(data,i,Math.random(),Math.random());
		}

		//          Form the FFTWComplex do do the work
		FFTWComplex fft = new FFTWComplex();


		//    Take out-of -place forward fft, does not alter the data[]
		//    array.
		double[] ft = fft.twoDimensional(width,height,data,1,false);
	       
		//    Take in place backwords fft  
		fft.twoDimensional(width,height,ft,-1,true);
		
		// Normalise...   (no default normalsiation when using FFTW)
		
		ArrayUtil.mult(ft,1.0/(width*height));

		//   Check difference by comparing the modulus of each element.
		double diff = 0;
		for(int i = 0; i < width*height; i++) {
		    double d = ArrayUtil.getComplex(data,i).modulus() -
			ArrayUtil.getComplex(ft,i).modulus();
		    diff += Math.abs(d);
		}
		
		System.out.println(diff);
		assertTrue(diff < 1e-7);
	}
	
	
	@Test
	public final void testTwoDimensionsFloat3() {
		
		System.out.println(TEST_STR + "testTwoDimensionsFloat3");
		
		int width = 200;                  // Size of array
		int height = 300;

		//             Make complex array (twice the size)
		float[] data = new float[2*width*height];

		//        Fill both parts with random using the ArrayUtil
		//        class to do the indexing.
		for(int i = 0; i < width*height; i=(i+2)) {
		    data[i] = (float)Math.random();
		    data[i+1] = (float)Math.random();
		}

		//          Form the FFTWComplex do do the work
		FFTWComplex fft = new FFTWComplex();


		//    Take out-of -place forward fft, does not alter the data[]
		//    array.
		float[] ft = fft.twoDimensional(width,height,data,1,false);
	       
		//    Take in place backwords fft  
		fft.twoDimensional(width,height,ft,-1,true);
		
		// Normalise...   (no default normalsiation when using FFTW)
		
		ArrayUtil.mult(ft,(float)(1.0/(width*height)));
		//for(int i = 0; i<ft.length; i++) {
		//    ft[i] = (ft[i]/(width*height));
		// }
		

		//   Check difference by comparing the modulus of each element.
		double diff = 0;
		for(int i = 0; i < width*height; i++) {
		    double d = ArrayUtil.getComplex(data,i).modulus() -
			ArrayUtil.getComplex(ft,i).modulus();
		    diff += Math.abs(d);
		}
		
		System.out.println(diff);
		assertTrue(diff < 1e-2);
	}
	
	
	
	@Test
	public final void testTwoDimensionsSplitDouble() {
		System.out.println(TEST_STR + "testTwoDimensionsSplitDouble");
		int width = 400;                // Non square array
		int height = 300;

		double[] rp = new double[width*height];  // Make real part.
		//      Fill the real space with random numbers
		for(int i = 0; i < rp.length; i++) {
		    rp[i] = Math.random();
		}

		//         Make interleaved array with null imaginary 
		//         The imaginary defaults to zero
		double[] dataInterleave = ArrayUtil.interleave(rp,null);
		
		//        Make a split array with specifed real and
		//        null imaginary, which defaults to zero
		double[][] dataSplit = ArrayUtil.split(rp,null);

		//         Make Complex FFT
		FFTWComplex fft = new FFTWComplex();

		//         FFT both formats, one in-place, other out-of-place
		//
		dataInterleave = fft.twoDimensional(width,height,dataInterleave,1,true);
		dataSplit = fft.twoDimensional(width,height,dataSplit,1,false);

		//         Form difference bwteeen modulus square (should be zero).
		double diff = 0.0;
		for(int i = 0; i < width*height; i++) {
		    double a = ArrayUtil.getComplex(dataInterleave,i).modulusSq();
		    double b = ArrayUtil.getComplex(dataSplit,i).modulusSq();
		    diff += (a - b);
		}
		
		System.out.println(diff);
		
		assertTrue(diff < 1e-6);
	    
	}

}
