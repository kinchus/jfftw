/*
 * Version 1.1 28/04/2013
 * Renombrado de tests unitarios 
 * 
 * Version 1.0 
 * 
 * � 2012 Jos� M. Garc�a Dom�nguez
 */
package jfftw.test;

import java.io.File;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import jfftw.FFTW;
import jfftw.FFTWReal;
import junit.framework.TestCase;

/**
 * 
 * @author jmgd6647
 */
public class Test04_MultithreadFFT 
extends TestCase {

	private String TEST_STR = getClass().getName() + "::";
	
	private String wisdomFilename = "wisdom.dat";
	private String wisdom = null;
	private FFTWReal fftwReal = null;
	
	
	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
		
		File fWisdom = null; 
		fftwReal = new FFTWReal();
		
		if (wisdomFilename != null) {

			fWisdom = new java.io.File( wisdomFilename );

			if (fWisdom.canRead()) {
				// Intenta cargar el fichero wisdom
				wisdom= FFTW.readWisdom( fWisdom );
			}
		}

	}

	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception {

		// Guarda los planes
		if ((wisdomFilename != null) && (fftwReal != null)) {
			
			File fWisdom = new java.io.File( wisdomFilename );

			if (fWisdom.canWrite()) {
				// Intenta cargar el fichero wisdom
				FFTW.writeWisdom(wisdom, fWisdom);
			}
		}

	}
	




	
	@Test
	public void testMultiThreadFFT() {

		System.out.println(TEST_STR + "testMultiThreadFFT");

		int numT = 50;
		int m=512;
		int n=1024;
		Thread runners [] = null;
		double [] a[] = null;
		
		runners = new Thread[numT];
		a = new double[numT][];
		
		for (int i=0;i<numT;i++) {
			a[i] = new double[m*n];
			runners[i] = new Thread(new FFTRunner(a[i], m, n));
			// System.out.println("\tThread FFTRunner id = " + runners[i].getId());
		}
			
		for (int i=0;i<numT;i++) {
			runners[i].start();
		}
		
		for (int i=0;i<numT;i++) {
			try {
				runners[i].join();
				System.out.print(".");
			} catch (InterruptedException e) {
				//
				e.printStackTrace();
			}
		}
		
		assertTrue(true);
	}
	
	
	
	
	/**
	 * Test method for {@link es.imgworks.classifier.datamanip.ImgArray#doFftR()}.
	 *
	@Test
	public void testTwoDimensionalFFT3() {

		System.out.println(TEST_STR + "testTwoDimensionalFFT3");

		// int [] dims = {7 ,7};
		
		double []a = new double[] {
                0, 1, 2, 3, 4, 5, 6, //
                7, 8, 9, 10, 11, 12, 13, //
                14, 15, 16, 17, 18, 19, 20, //
                21, 22, 23, 24, 25, 26, 27, //
                28, 29, 30, 31, 32, 33, 34, //
                35, 36, 37, 38, 39, 40, 41, //
                42, 43, 44, 45, 46, 47, 48 //
                };
     	double []b = new double[] {
                -1, 2, 2, -1, 0, 0, 0, //
                0, 1, 1, -4, 0, 0, 0, //
                0, 0, 1, -2, 0, 0, 0, //
                0, 0, 0, 1, 0, 0, 0, //
                0, 0, 0, 0, 0, 0, 0, //
                0, 0, 0, 0, 0, 0, 0, //
                0, 0, 0, 0, 0, 0, 0 //
                };
		
		double []expected = new double[] {
		
                -14, -14, -14, -14, 28, 0, -21, //
                -14, -14, -14, -14, 28, 0, -21, //
                -14, -14, -14, -14, 28, 0, -21, //
                -14, -14, -14, -14, 28, 0, -21, //
                -63, -63, -63, -63, -21, -49, -70, //
                -14, -14, -14, -14, 28, 0, -21, //
                84, 84, 84, 84, 126, 98, 77 //
                };
       
			
		double [] intermA = fftwReal.oneDimensionalForward(a);
		double [] intermB = fftwReal.oneDimensionalForward(b);
		
		double [] c = new double[7*7];
		
		
		
		return;
	}
	*/
	
	/**
	 * Performs a fft over an array of doubles.
	 * Array must be passed during construction
	 * 
	 * @author jmgarcia
	 * @version 1.0
	 */
	public class FFTRunner implements Runnable {
		
		private double [] data = null;
		private int m;
		private int n;

		public FFTRunner(double[] data, int m, int n) {
			this.data = data;
			this.m = m;
			this.n = n;
		}
		
		/**
		 * @see java.lang.Runnable#run()
		 */
		@Override
		public void run() {
			// 
			double []x = fftwReal.twoDimensionalForward(m, n, data);
			fftwReal.twoDimensionalBackward(m, n, x);
		}
		
	}
	
}
