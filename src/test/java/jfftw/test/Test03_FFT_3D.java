/*
 * Version 1.0 25/04/2013
 * 
 *
 * 2013 � Jos� M. Garc�a Dom�nguez
 *
 */
package jfftw.test;

import static org.junit.Assert.*;
import jfftw.ArrayUtil;
import jfftw.FFTWReal;

import org.junit.Before;
import org.junit.Test;


/**
 * @author jmgarcia
 * @version 1.0
 */
public class Test03_FFT_3D {

	private String TEST_STR = getClass().getName() + "::";
	
	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
	}

	@Test
	public final void testCubeDouble() {
		
		System.out.println(TEST_STR + "testCubeDouble");
				
		int width = 256;
		int height = 256;
		int depth = 128;

		//           Make a double array of currect size in real space
		double d[]= new double[width*height*depth];
		
		//          Set elements to random number between 0-10

		for(int i = 0; i < d.length; i++) {
		    d[i] = 10.0*Math.random();
		}

		//          Make the FFTWReal class to do the work
		FFTWReal fftw = new FFTWReal();

		//          Take out of place forward FFT, 
		//          return a double[] which hold Hermition 
		//          complex array in interleaved format.
		double ft[] = fftw.threeDimensionalForward(width,height,depth,d);
		
		//          Reconstruct, again out of place, back to real space
		double recon[] = fftw.threeDimensionalBackward(width,height,depth,ft);
		
		//          Scale the inverse by 1/N to (no scale in FFTW)
		//          Use ArrayUtil class to do the work.
		ArrayUtil.mult(recon,1.0/(width*height*depth));

		//          Calcualte diff square between d and recon
		double diff = 0.0;
		for(int i = 0; i < recon.length; i++) {
		    diff += Math.abs(d[i] - recon[i]);
		}

		//          Print out the difference
		//          (should be very small, about e-8)
		System.out.println(diff);
		
		assertTrue(diff < 1e-7);

		
	}
	
	
	@Test
	public final void testCubeFloat() {
		
		System.out.println(TEST_STR + "testCubeFloat");
				
		int width = 256;
		int height = 256;
		int depth = 128;

		//           Make a double array of currect size in real space
		float d[]= new float[width*height*depth];
		
		//          Set elements to random number between 0-10

		for(int i = 0; i < d.length; i++) {
		    d[i] = (float)(10.0*Math.random());
		}

		//          Make the FFTWReal class to do the work
		FFTWReal fftw = new FFTWReal();

		//          Take out of place forward FFT, 
		//          return a double[] which hold Hermition 
		//          complex array in interleaved format.
		float ft[] = fftw.threeDimensionalForward(width,height,depth,d);
		
		//          Reconstruct, again out of place, back to real space
		float recon[] = fftw.threeDimensionalBackward(width,height,depth,ft);
		
		//          Scale the inverse by 1/N to (no scale in FFTW)
		//          Use ArrayUtil class to do the work.
		ArrayUtil.mult(recon,(float)(1.0/(width*height*depth)));
		// for (int i=0;i<recon.length;i++) {
		//	recon[i] = (float)(recon[i]/(width*height*depth));
		// }

		//          Calcualte diff square between d and recon
		double diff = 0.0;
		for(int i = 0; i < recon.length; i++) {
		    diff += Math.abs(d[i] - recon[i]);
		}
		double meanDiff = (diff/recon.length);

		//          Print out the difference
		//          (should be very small, about e-8)
		System.out.println("DIFF: " + diff);
		System.out.println("Mean diff: " + meanDiff);
		
		assertTrue(meanDiff < 1e-5);

		
	}

}
