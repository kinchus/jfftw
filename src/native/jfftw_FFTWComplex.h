/* DO NOT EDIT THIS FILE - it is machine generated */
#include <jni.h>
/* Header for class jfftw_FFTWComplex */

#ifndef _Included_jfftw_FFTWComplex
#define _Included_jfftw_FFTWComplex
#ifdef __cplusplus
extern "C" {
#endif
#undef jfftw_FFTWComplex_REAL
#define jfftw_FFTWComplex_REAL 1L
#undef jfftw_FFTWComplex_FOURIER
#define jfftw_FFTWComplex_FOURIER 2L
#undef jfftw_FFTWComplex_FORWARD
#define jfftw_FFTWComplex_FORWARD 1L
#undef jfftw_FFTWComplex_BACKWARD
#define jfftw_FFTWComplex_BACKWARD -1L
#undef jfftw_FFTWComplex_EXHAUSTIVE
#define jfftw_FFTWComplex_EXHAUSTIVE 8L
#undef jfftw_FFTWComplex_PATIENT
#define jfftw_FFTWComplex_PATIENT 32L
#undef jfftw_FFTWComplex_ESTIMATE
#define jfftw_FFTWComplex_ESTIMATE 64L

	#define FFTW_COMPLEX	"jfftw/FFTWComplex"


/*
 * Class:     jfftw_FFTWComplex
 * Method:    nativeOneDimensional
 * Signature: ([D[DII)V
 */
JNIEXPORT void JNICALL Java_jfftw_FFTWComplex_nativeOneDimensional
  (JNIEnv *, jobject, jdoubleArray, jdoubleArray, jint, jint);

/*
 * Class:     jfftw_FFTWComplex
 * Method:    nativeOneDimensionalSplit
 * Signature: ([D[D[D[DI)V
 */
JNIEXPORT void JNICALL Java_jfftw_FFTWComplex_nativeOneDimensionalSplit
  (JNIEnv *, jobject, jdoubleArray, jdoubleArray, jdoubleArray, jdoubleArray, jint);

/*
 * Class:     jfftw_FFTWComplex
 * Method:    nativeTwoDimensional
 * Signature: (II[D[DII)V
 */
JNIEXPORT void JNICALL Java_jfftw_FFTWComplex_nativeTwoDimensional
  (JNIEnv *, jobject, jint, jint, jdoubleArray, jdoubleArray, jint, jint);

/*
 * Class:     jfftw_FFTWComplex
 * Method:    nativeTwoDimensionalSplit
 * Signature: (II[D[D[D[DI)V
 */
JNIEXPORT void JNICALL Java_jfftw_FFTWComplex_nativeTwoDimensionalSplit
  (JNIEnv *, jobject, jint, jint, jdoubleArray, jdoubleArray, jdoubleArray, jdoubleArray, jint);

/*
 * Class:     jfftw_FFTWComplex
 * Method:    nativeThreeDimensional
 * Signature: (III[D[DII)V
 */
JNIEXPORT void JNICALL Java_jfftw_FFTWComplex_nativeThreeDimensional
  (JNIEnv *, jobject, jint, jint, jint, jdoubleArray, jdoubleArray, jint, jint);

/*
 * Class:     jfftw_FFTWComplex
 * Method:    nativeThreeDimensionalSplit
 * Signature: (III[D[D[D[DI)V
 */
JNIEXPORT void JNICALL Java_jfftw_FFTWComplex_nativeThreeDimensionalSplit
  (JNIEnv *, jobject, jint, jint, jint, jdoubleArray, jdoubleArray, jdoubleArray, jdoubleArray, jint);


/*
 * Class:     jfftw_FFTWComplex
 * Method:    nativeOneDimensional
 * Signature: ([D[DII)V
 */
JNIEXPORT void JNICALL Java_jfftw_FFTWComplex_nativeOneDimensionalF
  (JNIEnv *, jobject, jfloatArray, jfloatArray, jint, jint);

/*
 * Class:     jfftw_FFTWComplex
 * Method:    nativeOneDimensionalSplit
 * Signature: ([D[D[D[DI)V
 */
JNIEXPORT void JNICALL Java_jfftw_FFTWComplex_nativeOneDimensionalSplitF
  (JNIEnv *, jobject, jfloatArray, jfloatArray, jfloatArray, jfloatArray, jint);

/*
 * Class:     jfftw_FFTWComplex
 * Method:    nativeTwoDimensional
 * Signature: (II[D[DII)V
 */
JNIEXPORT void JNICALL Java_jfftw_FFTWComplex_nativeTwoDimensionalF
  (JNIEnv *, jobject, jint, jint, jfloatArray, jfloatArray, jint, jint);

/*
 * Class:     jfftw_FFTWComplex
 * Method:    nativeTwoDimensionalSplit
 * Signature: (II[D[D[D[DI)V
 */
JNIEXPORT void JNICALL Java_jfftw_FFTWComplex_nativeTwoDimensionalSplitF
  (JNIEnv *, jobject, jint, jint, jfloatArray, jfloatArray, jfloatArray, jfloatArray, jint);

/*
 * Class:     jfftw_FFTWComplex
 * Method:    nativeThreeDimensional
 * Signature: (III[D[DII)V
 */
JNIEXPORT void JNICALL Java_jfftw_FFTWComplex_nativeThreeDimensionalF
  (JNIEnv *, jobject, jint, jint, jint, jfloatArray, jfloatArray, jint, jint);

/*
 * Class:     jfftw_FFTWComplex
 * Method:    nativeThreeDimensionalSplit
 * Signature: (III[D[D[D[DI)V
 */
JNIEXPORT void JNICALL Java_jfftw_FFTWComplex_nativeThreeDimensionalSplitF
  (JNIEnv *, jobject, jint, jint, jint, jfloatArray, jfloatArray, jfloatArray, jfloatArray, jint);





#ifdef __cplusplus
}
#endif
#endif
