/*       C implementation of the FFTWComplex native methods
 *       Author Will Hossacl, 2008
 */
#include "jnimacros.h"
#include "jfftw_FFTW.h"
#include "jfftw_FFTWComplex.h"
#include <fftw3.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#if _MSC_VER
#define snprintf _snprintf
#endif

/*
 *    This file is part of jfftw.
 * 
 *    Jfftw is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    Jfftw is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with Jfftw.  If not, see <http://www.gnu.org/licenses/>.
 */

/*           nativeOneDimensional
 */

JNIEXPORT void JNICALL Java_jfftw_FFTWComplex_nativeOneDimensional
(JNIEnv *env, jobject obj, jdoubleArray in, jdoubleArray out, jint dirn, jint flag){

  
  double *inptr, *outptr;                     // input and output array pointers
  int length;                                 // Transform length
  fftw_plan  plan;                            // the plan

  inptr = GetDoubleArray(in);                // Get input array
  outptr = GetDoubleArray(out);              // get output array
  length = ArrayLength(in)/2;                // Half length needed

// Creates globlal reference to the monitor that controls exclusive access
	// to plan creation function
	if (fftw_monitor == NULL) {

		// Verifica la referencia
		if (FFTWComplex_class == NULL) {
			FFTWComplex_class = get_class_reference_WG(env, FFTW_COMPLEX);
		}

		// Obtiene la referencia al monitor
		fftw_monitor = get_static_monitor_WG(env, FFTWComplex_class, FFTW_MONITOR);
	}

	if (fftw_monitor == NULL) {
		char buffer[256];
		const char* error = "get_static_monitor_WG(): Error al obtener la referencia del objeto monitor %s";
		snprintf(buffer, strlen(FFTW_MONITOR) + strlen(error), error, FFTW_MONITOR);
		(*env)->FatalError(env, buffer);
	}

	// ENtra en zona de exclusi�n
	if ((*env)->MonitorEnter(env, fftw_monitor) < 0) {
		(*env)->FatalError(env, "Error al tratar de ganar acceso exclusivo al objeto monitor");
	}

	//          Make a simple plan using flag (default to ESTIMATE)
	plan = fftw_plan_dft_1d(length, (fftw_complex*)inptr,
							  (fftw_complex*)outptr,dirn,
							  (unsigned int)flag);
	(*env)->MonitorExit(env, fftw_monitor);


	fftw_execute(plan);                       // Do the FFT
	fftw_destroy_plan(plan);                  // Clear up
  
	ReleaseDoubleArray(in,inptr);             // Release the arrays
	ReleaseDoubleArray(out,outptr);
}

/*         nativeOneDimensinalSplit
 */
JNIEXPORT void JNICALL Java_jfftw_FFTWComplex_nativeOneDimensionalSplit
  (JNIEnv *env, jobject obj, jdoubleArray realin, jdoubleArray imagin, 
   jdoubleArray realout, jdoubleArray imagout , jint flag){

  double *realinptr, *imaginptr, *realoutptr, *imagoutptr;
  fftw_plan plan;
  fftw_iodim dim;
  
  realinptr = GetDoubleArray(realin);        // Get real input array
  imaginptr = GetDoubleArray(imagin);        // Get imag input array
  realoutptr = GetDoubleArray(realout);      // Get real out array
  imagoutptr = GetDoubleArray(imagout);      // Get imag out array;
  dim.n = ArrayLength(realin);               // Array length (all the same)
  dim.is = 1;                                // array span is 1
  dim.os = 1;
  
  	// Creates globlal reference to the monitor that controls exclusive access
	// to plan creation function
	if (fftw_monitor == NULL) {

		// Verifica la referencia
		if (FFTWComplex_class == NULL) {
			FFTWComplex_class = get_class_reference_WG(env, FFTW_COMPLEX);
		}

		// Obtiene la referencia al monitor
		fftw_monitor = get_static_monitor_WG(env, FFTWComplex_class, FFTW_MONITOR);
	}

	if (fftw_monitor == NULL) {
		char buffer[256];
		const char* error = "get_static_monitor_WG(): Error al obtener la referencia del objeto monitor %s";
		snprintf(buffer, strlen(FFTW_MONITOR) + strlen(error), error, FFTW_MONITOR);
		(*env)->FatalError(env, buffer);
	}

	// ENtra en zona de exclusi�n
	if ((*env)->MonitorEnter(env, fftw_monitor) < 0) {
		(*env)->FatalError(env, "Error al tratar de ganar acceso exclusivo al objeto monitor");
	}

	//      Make plan using guru interface for split array
	plan = fftw_plan_guru_split_dft(1,&dim,0,&dim,
				  realinptr,imaginptr,
				  realoutptr,imagoutptr,
				  (unsigned int)flag);
	(*env)->MonitorExit(env, fftw_monitor);


  fftw_execute(plan);                       // Do the FFT
  fftw_destroy_plan(plan);                  // Clear up
  
  ReleaseDoubleArray(realin,realinptr);             // Release the arrays
  ReleaseDoubleArray(imagin,imaginptr);
  ReleaseDoubleArray(realout,realoutptr);
  ReleaseDoubleArray(imagout,imagoutptr);
}

/*        nativeTwoDimensional
 */
JNIEXPORT void JNICALL Java_jfftw_FFTWComplex_nativeTwoDimensional
(JNIEnv *env, jobject obj, jint width, jint height, jdoubleArray in, jdoubleArray out, jint dirn, jint flag){


  double *inptr, *outptr;                     // input and output array pointers
  fftw_plan  plan;                            // the plan

  inptr = GetDoubleArray(in);                // Get input array
  outptr = GetDoubleArray(out);              // get output array

  // Creates globlal reference to the monitor that controls exclusive access
	// to plan creation function
	if (fftw_monitor == NULL) {

		// Verifica la referencia
		if (FFTWComplex_class == NULL) {
			FFTWComplex_class = get_class_reference_WG(env, FFTW_COMPLEX);
		}

		// Obtiene la referencia al monitor
		fftw_monitor = get_static_monitor_WG(env, FFTWComplex_class, FFTW_MONITOR);
	}

	if (fftw_monitor == NULL) {
		char buffer[256];
		const char* error = "get_static_monitor_WG(): Error al obtener la referencia del objeto monitor %s";
		snprintf(buffer, strlen(FFTW_MONITOR) + strlen(error), error, FFTW_MONITOR);
		(*env)->FatalError(env, buffer);
	}

	// ENtra en zona de exclusi�n
	if ((*env)->MonitorEnter(env, fftw_monitor) < 0) {
		(*env)->FatalError(env, "Error al tratar de ganar acceso exclusivo al objeto monitor");
	}

	//      Make plan using guru interface for split array
  plan = fftw_plan_dft_2d(height,width, (fftw_complex*)inptr,
                          (fftw_complex*)outptr,dirn,
                          (unsigned int)flag);
  (*env)->MonitorExit(env, fftw_monitor);


  fftw_execute(plan);                       // Do the FFT
  fftw_destroy_plan(plan);                  // Clear up

  ReleaseDoubleArray(in,inptr);             // Release the arrays
  ReleaseDoubleArray(out,outptr);
}

/*     nativeTwoDimensinalSplit
 */
JNIEXPORT void JNICALL Java_jfftw_FFTWComplex_nativeTwoDimensionalSplit
  (JNIEnv *env, jobject obj, jint width, jint height, jdoubleArray realin, jdoubleArray imagin, 
   jdoubleArray realout, jdoubleArray imagout, jint flag) {
  
  double *realinptr, *imaginptr, *realoutptr, *imagoutptr;
  fftw_plan plan;
  fftw_iodim *dim = (fftw_iodim*)(malloc(sizeof(fftw_iodim)*2));
  
  realinptr = GetDoubleArray(realin);        // Get real input array
  imaginptr = GetDoubleArray(imagin);        // Get imag input array
  realoutptr = GetDoubleArray(realout);      // Get real out array
  imagoutptr = GetDoubleArray(imagout);      // Get imag out array;

  dim[0].n = width;                         // First dimension
  dim[0].is = 1;                            // First dimension in span
  dim[0].os = 1;                            // First dimension out span  
  dim[1].n = height;                        // Second dimension
  dim[1].is = width;                        // Secpnd dikension in span
  dim[1].os = width;                        // Second dimension out span

   // Creates globlal reference to the monitor that controls exclusive access
	// to plan creation function
	if (fftw_monitor == NULL) {

		// Verifica la referencia
		if (FFTWComplex_class == NULL) {
			FFTWComplex_class = get_class_reference_WG(env, FFTW_COMPLEX);
		}

		// Obtiene la referencia al monitor
		fftw_monitor = get_static_monitor_WG(env, FFTWComplex_class, FFTW_MONITOR);
	}

	if (fftw_monitor == NULL) {
		char buffer[256];
		const char* error = "get_static_monitor_WG(): Error al obtener la referencia del objeto monitor %s";
		snprintf(buffer, strlen(FFTW_MONITOR) + strlen(error), error, FFTW_MONITOR);
		(*env)->FatalError(env, buffer);
	}

	// ENtra en zona de exclusi�n
	if ((*env)->MonitorEnter(env, fftw_monitor) < 0) {
		(*env)->FatalError(env, "Error al tratar de ganar acceso exclusivo al objeto monitor");
	}
  //      Make plan using guru interface for two dimensional split array
  plan = fftw_plan_guru_split_dft(2,dim,0,dim,
				  realinptr,imaginptr,
				  realoutptr,imagoutptr,
				  (unsigned int)flag);
  (*env)->MonitorExit(env, fftw_monitor);

  fftw_execute(plan);                       // Do the FFT
  fftw_destroy_plan(plan);                  // Clear up
  free(dim);
  
  ReleaseDoubleArray(realin,realinptr);             // Release the arrays
  ReleaseDoubleArray(imagin,imaginptr);
  ReleaseDoubleArray(realout,realoutptr);
  ReleaseDoubleArray(imagout,imagoutptr);
}



/*     nativeThreeDimensional
 */
JNIEXPORT void JNICALL Java_jfftw_FFTWComplex_nativeThreeDimensional
  (JNIEnv *env, jobject obj, jint width, jint height, jint depth, 
   jdoubleArray in, jdoubleArray out, jint dirn, jint flag){


  double *inptr, *outptr;                     // input and output array pointers
  fftw_plan  plan;                            // the plan

  inptr = GetDoubleArray(in);                // Get input array
  outptr = GetDoubleArray(out);              // get output array

  // Creates globlal reference to the monitor that controls exclusive access
	// to plan creation function
	if (fftw_monitor == NULL) {

		// Verifica la referencia
		if (FFTWComplex_class == NULL) {
			FFTWComplex_class = get_class_reference_WG(env, FFTW_COMPLEX);
		}

		// Obtiene la referencia al monitor
		fftw_monitor = get_static_monitor_WG(env, FFTWComplex_class, FFTW_MONITOR);
	}

	if (fftw_monitor == NULL) {
		char buffer[256];
		const char* error = "get_static_monitor_WG(): Error al obtener la referencia del objeto monitor %s";
		snprintf(buffer, strlen(FFTW_MONITOR) + strlen(error), error, FFTW_MONITOR);
		(*env)->FatalError(env, buffer);
	}

	// ENtra en zona de exclusi�n
	if ((*env)->MonitorEnter(env, fftw_monitor) < 0) {
		(*env)->FatalError(env, "Error al tratar de ganar acceso exclusivo al objeto monitor");
	}

  //          Make a simple plan using flag (normall ESTIMATE)
  plan = fftw_plan_dft_3d(depth,height,width, (fftw_complex*)inptr,
                          (fftw_complex*)outptr,dirn,(unsigned int)flag);
  (*env)->MonitorExit(env, fftw_monitor);


  fftw_execute(plan);                       // Do the FFT
  fftw_destroy_plan(plan);                  // Clear up

  ReleaseDoubleArray(in,inptr);             // Release the arrays
  ReleaseDoubleArray(out,outptr);
}


/*      nativeThreeDimsneionalSplit
 */
JNIEXPORT void JNICALL Java_jfftw_FFTWComplex_nativeThreeDimensionalSplit
  (JNIEnv *env, jobject obj, jint width, jint height , jint depth, 
   jdoubleArray realin, jdoubleArray imagin, jdoubleArray realout, jdoubleArray imagout, jint flag){

  
  double *realinptr, *imaginptr, *realoutptr, *imagoutptr;
  fftw_plan plan;
  fftw_iodim *dim = (fftw_iodim*)(malloc(sizeof(fftw_iodim)*3));
  
  realinptr = GetDoubleArray(realin);        // Get real input array
  imaginptr = GetDoubleArray(imagin);        // Get imag input array
  realoutptr = GetDoubleArray(realout);      // Get real out array
  imagoutptr = GetDoubleArray(imagout);      // Get imag out array;

  dim[0].n = width;                         // First dimension
  dim[0].is = 1;                            // First dimension in span
  dim[0].os = 1;                            // First dimension out span  
  dim[1].n = height;                        // Second dimension
  dim[1].is = width;                        // Secpnd dimension in span
  dim[1].os = width;                        // Second dimension out span
  dim[2].n = depth;                         // Third dimension
  dim[2].is = width*height;                 // Third dimension in span
  dim[2].os = width*height;                 // Third dimension out span

  // Creates globlal reference to the monitor that controls exclusive access
	// to plan creation function
	if (fftw_monitor == NULL) {

		// Verifica la referencia
		if (FFTWComplex_class == NULL) {
			FFTWComplex_class = get_class_reference_WG(env, FFTW_COMPLEX);
		}

		// Obtiene la referencia al monitor
		fftw_monitor = get_static_monitor_WG(env, FFTWComplex_class, FFTW_MONITOR);
	}

	if (fftw_monitor == NULL) {
		char buffer[256];
		const char* error = "get_static_monitor_WG(): Error al obtener la referencia del objeto monitor %s";
		snprintf(buffer, strlen(FFTW_MONITOR) + strlen(error), error, FFTW_MONITOR);
		(*env)->FatalError(env, buffer);
	}

	// ENtra en zona de exclusi�n
	if ((*env)->MonitorEnter(env, fftw_monitor) < 0) {
		(*env)->FatalError(env, "Error al tratar de ganar acceso exclusivo al objeto monitor");
	}
  //      Make plan using guru interface for three dimensional split array
  plan = fftw_plan_guru_split_dft(3,dim,0,dim,
				  realinptr,imaginptr,
				  realoutptr,imagoutptr,
				  (unsigned int)flag);
  (*env)->MonitorExit(env, fftw_monitor);

  fftw_execute(plan);                       // Do the FFT
  fftw_destroy_plan(plan);                  // Clear up
  free(dim);
  
  ReleaseDoubleArray(realin,realinptr);             // Release the arrays
  ReleaseDoubleArray(imagin,imaginptr);
  ReleaseDoubleArray(realout,realoutptr);
  ReleaseDoubleArray(imagout,imagoutptr);
}
