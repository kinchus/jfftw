/*       C implementation of the FFTWReal natives.
 *       Author Will Hossacl, 2008
 */
#include "jnimacros.h"
#include "jfftw_FFTW.h"
#include "jfftw_FFTWReal.h"
#include <fftw3.h>
#include <stdio.h>
#include <string.h>

#if _MSC_VER
#define snprintf _snprintf
#endif

/*
 *    This file is part of jfftw.
 *
 *    Jfftw is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    Jfftw is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with Jfftw.  If not, see <http://www.gnu.org/licenses/>.
 */


/*               nativeOneDimensionalForward
 */

JNIEXPORT void JNICALL Java_jfftw_FFTWReal_nativeOneDimensionalForward
(JNIEnv *env, jobject obj, jdoubleArray in, jdoubleArray out, jint flag){

  
  double *inptr, *outptr;                     // input and output array pointers  
  int length;                                 // Transform length
  fftw_plan  plan;                            // the plan	

  inptr = GetDoubleArray(in);                // Get input array
  outptr = GetDoubleArray(out);              // get output array
  length = ArrayLength(in);                  // Length of transform


  // Creates globlal reference to the monitor that controls exclusive access
	// to plan creation function
	if (fftw_monitor == NULL) {

		// Verifica la referencia
		if (FFTWReal_class == NULL) {
			FFTWReal_class = get_class_reference_WG(env, FFTW_REAL);
		}

		// Obtiene la referencia al monitor
		fftw_monitor = get_static_monitor_WG(env, FFTWReal_class, FFTW_MONITOR);
	}

	if (fftw_monitor == NULL) {
		char buffer[256];
		const char* error = "get_static_monitor_WG(): Error al obtener la referencia del objeto monitor %s";
		snprintf(buffer, strlen(FFTW_MONITOR) + strlen(error), error, FFTW_MONITOR);
		(*env)->FatalError(env, buffer);
	}


	// ENtra en zona de exclusi�n
	if ((*env)->MonitorEnter(env, fftw_monitor) < 0) {
		(*env)->FatalError(env, "Error al tratar de ganar acceso exclusivo al objeto monitor");
	}
	plan = fftw_plan_dft_r2c_1d(length,inptr,(fftw_complex*)outptr,
			      (unsigned int)flag);
	(*env)->MonitorExit(env, fftw_monitor);

  
	fftw_execute(plan);                       // Do the FFT
	
	// ENtra en zona de exclusi�n
	if ((*env)->MonitorEnter(env, fftw_monitor) < 0) {
		(*env)->FatalError(env, "Error al tratar de ganar acceso exclusivo al objeto monitor");
	}
	fftw_destroy_plan(plan);                  // Clear up
	(*env)->MonitorExit(env, fftw_monitor);

 
	ReleaseDoubleArray(in,inptr);             // Release the arrays
	ReleaseDoubleArray(out,outptr);
}


/*               nativeOneDimensionalBackward
 */

JNIEXPORT void JNICALL Java_jfftw_FFTWReal_nativeOneDimensionalBackward
(JNIEnv *env, jobject obj, jdoubleArray in, jdoubleArray out, jint flag){

  double *inptr, *outptr;                     // input and output array pointers  
  int length;                                 // Transform length
  fftw_plan  plan;                            // the plan
  	
  inptr = GetDoubleArray(in);                // Get input array
  outptr = GetDoubleArray(out);              // get output array
  length = ArrayLength(out);                  // Length of transform (output)

    // Creates globlal reference to the monitor that controls exclusive access
	// to plan creation function
	if (fftw_monitor == NULL) {

		// Verifica la referencia
		if (FFTWReal_class == NULL) {
			FFTWReal_class = get_class_reference_WG(env, FFTW_REAL);
		}

		// Obtiene la referencia al monitor
		fftw_monitor = get_static_monitor_WG(env, FFTWReal_class, FFTW_MONITOR);
	}

	if (fftw_monitor == NULL) {
		char buffer[256];
		const char* error = "get_static_monitor_WG(): Error al obtener la referencia del objeto monitor %s";
		snprintf(buffer, strlen(FFTW_MONITOR) + strlen(error), error, FFTW_MONITOR);
		(*env)->FatalError(env, buffer);
	}

	// ENtra en zona de exclusi�n
	if ((*env)->MonitorEnter(env, fftw_monitor) < 0) {
		(*env)->FatalError(env, "Error al tratar de ganar acceso exclusivo al objeto monitor");
	}
	plan = fftw_plan_dft_c2r_1d(length,(fftw_complex*)inptr,outptr,
			      (unsigned int)flag);
	(*env)->MonitorExit(env, fftw_monitor);


  fftw_execute(plan);                       // Do the FFT
  
  // ENtra en zona de exclusi�n
  if ((*env)->MonitorEnter(env, fftw_monitor) < 0) {
	(*env)->FatalError(env, "Error al tratar de ganar acceso exclusivo al objeto monitor");
  }
  fftw_destroy_plan(plan);                  // Clear up
  (*env)->MonitorExit(env, fftw_monitor);
  

  ReleaseDoubleArray(in,inptr);             // Release the arrays
  ReleaseDoubleArray(out,outptr);
}

/*                            twoDimensionaForward
 */
JNIEXPORT void JNICALL Java_jfftw_FFTWReal_nativeTwoDimensionalForward
(JNIEnv *env , jobject obj, jint width, jint height, jdoubleArray in, jdoubleArray out, jint flag){

	double *inptr, *outptr;                     // input and output array pointers  
	fftw_plan  plan;                            // the plan
	
	inptr = GetDoubleArray(in);                // Get input array
	outptr = GetDoubleArray(out);              // get output array

	// Creates globlal reference to the monitor that controls exclusive access
	// to plan creation function
	if (fftw_monitor == NULL) {

		// Verifica la referencia
		if (FFTWReal_class == NULL) {
			FFTWReal_class = get_class_reference_WG(env, FFTW_REAL);
		}

		// Obtiene la referencia al monitor
		fftw_monitor = get_static_monitor_WG(env, FFTWReal_class, FFTW_MONITOR);
	}

	// ENtra en zona de exclusi�n
	if ((*env)->MonitorEnter(env, fftw_monitor) < 0) {
		char buffer[256];
		const char* error = "get_static_monitor_WG(): Error al obtener la referencia del objeto monitor %s";
		snprintf(buffer, strlen(FFTW_MONITOR) + strlen(error), error, FFTW_MONITOR);
		(*env)->FatalError(env, buffer);
	}
	// Make the two-d plan
	plan = fftw_plan_dft_r2c_2d(height,width,inptr,
					(fftw_complex*)outptr,
					(unsigned int)flag);

	(*env)->MonitorExit(env, fftw_monitor);
  
	fftw_execute(plan);                       // Do the FFT

	// ENtra en zona de exclusi�n
	if ((*env)->MonitorEnter(env, fftw_monitor) < 0) {
		(*env)->FatalError(env, "Error al tratar de ganar acceso exclusivo al objeto monitor");
	}
	fftw_destroy_plan(plan);                  // Clear up
	(*env)->MonitorExit(env, fftw_monitor);
  
	ReleaseDoubleArray(in,inptr);             // Release the arrays
	ReleaseDoubleArray(out,outptr);
}


/*                            twoDimensionaBackward
 */
JNIEXPORT void JNICALL Java_jfftw_FFTWReal_nativeTwoDimensionalBackward
(JNIEnv *env , jobject obj, jint width, jint height, jdoubleArray in, jdoubleArray out, jint flag){

  
  double *inptr, *outptr;                     // input and output array pointers  
  fftw_plan  plan;                            // the plan

  inptr = GetDoubleArray(in);                // Get input array
  outptr = GetDoubleArray(out);              // get output array

	// Creates globlal reference to the monitor that controls exclusive access
	// to plan creation function
	if (fftw_monitor == NULL) {
		// Verifica la referencia
		if (FFTWReal_class == NULL) {
			FFTWReal_class = get_class_reference_WG(env, FFTW_REAL);
		}
		
		// Obtiene la referencia al monitor
		fftw_monitor = get_static_monitor_WG(env, FFTWReal_class, FFTW_MONITOR);
	}

	// Verifica el monitor
	if (fftw_monitor == NULL) {
		char buffer[256];
		const char* error = "get_static_monitor_WG(): Error al obtener la referencia del objeto monitor %s";
		snprintf(buffer, strlen(FFTW_MONITOR) + strlen(error), error, FFTW_MONITOR);
		(*env)->FatalError(env, buffer);
	}

	// ENtra en zona de exclusi�n
	if ((*env)->MonitorEnter(env, fftw_monitor) < 0) {
		(*env)->FatalError(env, "Error al tratar de ganar acceso exclusivo al objeto monitor");
	}
	plan = fftw_plan_dft_c2r_2d(height,width,(fftw_complex*)inptr,
			      outptr,(unsigned int)flag);
	(*env)->MonitorExit(env, fftw_monitor);

  
	fftw_execute(plan);                       // Do the FFT


	// ENtra en zona de exclusi�n
	if ((*env)->MonitorEnter(env, fftw_monitor) < 0) {
		(*env)->FatalError(env, "Error al tratar de ganar acceso exclusivo al objeto monitor");
	}
	fftw_destroy_plan(plan);                  // Clear up
	(*env)->MonitorExit(env, fftw_monitor);
  
	
	ReleaseDoubleArray(in,inptr);             // Release the arrays
	ReleaseDoubleArray(out,outptr);
}


/*                         threeDimensionalForward
 */
JNIEXPORT void JNICALL Java_jfftw_FFTWReal_nativeThreeDimensionalForward
(JNIEnv *env, jobject obj, jint width, jint height, jint depth, 
 jdoubleArray in, jdoubleArray out, jint flag) {

	double *inptr, *outptr;                    // input and output array pointers
	fftw_plan plan;                            // the plan
	
	inptr = GetDoubleArray(in);                // Get input array
	outptr = GetDoubleArray(out);              // get output array

	// Creates globlal reference to the monitor that controls exclusive access
	// to plan creation function
	if (fftw_monitor == NULL) {

		// Verifica la referencia
		if (FFTWReal_class == NULL) {
			FFTWReal_class = get_class_reference_WG(env, FFTW_REAL);
		}

		// Obtiene la referencia al monitor
		fftw_monitor = get_static_monitor_WG(env, FFTWReal_class, FFTW_MONITOR);
	}

	if (fftw_monitor == NULL) {
		char buffer[256];
		const char* error = "get_static_monitor_WG(): Error al obtener la referencia del objeto monitor %s";
		snprintf(buffer, strlen(FFTW_MONITOR) + strlen(error), error, FFTW_MONITOR);
		(*env)->FatalError(env, buffer);
	}

	// Entra en zona de exclusi�n
	if ((*env)->MonitorEnter(env, fftw_monitor) < 0) {
		(*env)->FatalError(env, "get_static_monitor_WG(): Error al tratar de ganar acceso exclusivo al objeto monitor");
	}
	//                    Make the three-d plan
	plan = fftw_plan_dft_r2c_3d(depth,height,width,inptr,
			      (fftw_complex*)outptr,
			      (unsigned int)flag);
	(*env)->MonitorExit(env, fftw_monitor);
  
  
	fftw_execute(plan);                       // Do the FFT

	if ((*env)->MonitorEnter(env, fftw_monitor) < 0) {
		(*env)->FatalError(env, "Error al tratar de ganar acceso exclusivo al objeto monitor");
	}
	fftw_destroy_plan(plan);                  // Clear up
	(*env)->MonitorExit(env, fftw_monitor);


	ReleaseDoubleArray(in,inptr);             // Release the arrays
	ReleaseDoubleArray(out,outptr);
}

/*                            threeDimensionaBackward
 */
JNIEXPORT void JNICALL Java_jfftw_FFTWReal_nativeThreeDimensionalBackward
(JNIEnv *env , jobject obj, jint width, jint height, jint depth, jdoubleArray in, jdoubleArray out, jint flag){
  
  double *inptr, *outptr;                     // input and output array pointers  
  fftw_plan  plan;                            // the plan
  
  inptr = GetDoubleArray(in);                // Get input array
  outptr = GetDoubleArray(out);              // get output array

  
  	// Creates globlal reference to the monitor that controls exclusive access
	// to plan creation function
	if (fftw_monitor == NULL) {

		// Verifica la referencia
		if (FFTWReal_class == NULL) {
			FFTWReal_class = get_class_reference_WG(env, FFTW_REAL);
		}

		fftw_monitor = get_static_monitor_WG(env, FFTWReal_class, FFTW_MONITOR);
	}

	if (fftw_monitor == NULL) {
		char buffer[256];
		const char* error = "get_static_monitor_WG(): Error al obtener la referencia del objeto monitor %s";
		snprintf(buffer, strlen(FFTW_MONITOR) + strlen(error), error, FFTW_MONITOR);
		(*env)->FatalError(env, buffer);
	}

	// Entra en zona de exclusi�n
	if ((*env)->MonitorEnter(env, fftw_monitor) < 0) {
		(*env)->FatalError(env, "Error al tratar de ganar acceso exclusivo al objeto monitor");
	}
	//                    Make the two-d plan
  plan = fftw_plan_dft_c2r_3d(depth,height,width,(fftw_complex*)inptr,
			      outptr,(unsigned int)flag);
  (*env)->MonitorExit(env, fftw_monitor);
  
  
  fftw_execute(plan);                       // Do the FFT
  
  if ((*env)->MonitorEnter(env, fftw_monitor) < 0) {
		(*env)->FatalError(env, "Error al tratar de ganar acceso exclusivo al objeto monitor");
  }
  fftw_destroy_plan(plan);                  // Clear 
  (*env)->MonitorExit(env, fftw_monitor);


  ReleaseDoubleArray(in,inptr);             // Release the arrays
  ReleaseDoubleArray(out,outptr);
}


