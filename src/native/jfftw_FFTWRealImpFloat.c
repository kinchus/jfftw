/*       C implementation of the FFTWReal natives.
 *       Author Will Hossacl, 2008
 */
#include "jnimacros.h"
#include "jfftw_FFTW.h"
#include "jfftw_FFTWReal.h"
#include <fftw3.h>
#include <stdio.h>
#include <string.h>

#if _MSC_VER
#define snprintf _snprintf
#endif

/*
 *    This file is part of jfftw.
 *
 *    Jfftw is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    Jfftw is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with Jfftw.  If not, see <http://www.gnu.org/licenses/>.
 */


/*               nativeOneDimensionalForwardF
 */

JNIEXPORT void JNICALL Java_jfftw_FFTWReal_nativeOneDimensionalForwardF
(JNIEnv *env, jobject obj, jfloatArray in, jfloatArray out, jint flag) {
	
  
  float *inptr, *outptr;                     // input and output array pointers  
  int length;                                 // Transform length
  fftwf_plan  plan;                            // the plan	

  inptr = GetFloatArray(in);                // Get input array
  outptr = GetFloatArray(out);              // get output array
  length = ArrayLength(in);                  // Length of transform


  // Creates globlal reference to the monitor that controls exclusive access
	// to plan creation function
	if (fftw_monitor == NULL) {

		// Verifica la referencia
		if (FFTWReal_class == NULL) {
			FFTWReal_class = get_class_reference_WG(env, FFTW_REAL);
		}

		// Obtiene la referencia al monitor
		fftw_monitor = get_static_monitor_WG(env, FFTWReal_class, FFTW_MONITOR);
	}

	if (fftw_monitor == NULL) {
		char buffer[256];
		const char* error = "get_static_monitor_WG(): Error al obtener la referencia del objeto monitor %s";
		snprintf(buffer, strlen(FFTW_MONITOR) + strlen(error), error, FFTW_MONITOR);
		(*env)->FatalError(env, buffer);
	}


	// ENtra en zona de exclusi�n
	if ((*env)->MonitorEnter(env, fftw_monitor) < 0) {
		(*env)->FatalError(env, "Error al tratar de ganar acceso exclusivo al objeto monitor");
	}
	plan = fftwf_plan_dft_r2c_1d(length,inptr,(fftwf_complex*)outptr,
			      (unsigned int)flag);
	(*env)->MonitorExit(env, fftw_monitor);

  
	fftwf_execute(plan);                       // Do the FFT
	
	// ENtra en zona de exclusi�n
	if ((*env)->MonitorEnter(env, fftw_monitor) < 0) {
		(*env)->FatalError(env, "Error al tratar de ganar acceso exclusivo al objeto monitor");
	}
	fftwf_destroy_plan(plan);                  // Clear up
	(*env)->MonitorExit(env, fftw_monitor);

 
	ReleaseFloatArray(in,inptr);             // Release the arrays
	ReleaseFloatArray(out,outptr);
}


/*               nativeOneDimensionalBackwardF
 */

JNIEXPORT void JNICALL Java_jfftw_FFTWReal_nativeOneDimensionalBackwardF
(JNIEnv *env, jobject obj, jfloatArray in, jfloatArray out, jint flag){

  float *inptr, *outptr;                     // input and output array pointers  
  int length;                                 // Transform length
  fftwf_plan  plan;                            // the plan
  	
  inptr = GetFloatArray(in);                // Get input array
  outptr = GetFloatArray(out);              // get output array
  length = ArrayLength(out);                  // Length of transform (output)

    // Creates globlal reference to the monitor that controls exclusive access
	// to plan creation function
	if (fftw_monitor == NULL) {

		// Verifica la referencia
		if (FFTWReal_class == NULL) {
			FFTWReal_class = get_class_reference_WG(env, FFTW_REAL);
		}

		// Obtiene la referencia al monitor
		fftw_monitor = get_static_monitor_WG(env, FFTWReal_class, FFTW_MONITOR);
	}

	if (fftw_monitor == NULL) {
		char buffer[256];
		const char* error = "get_static_monitor_WG(): Error al obtener la referencia del objeto monitor %s";
		snprintf(buffer, strlen(FFTW_MONITOR) + strlen(error), error, FFTW_MONITOR);
		(*env)->FatalError(env, buffer);
	}

	// ENtra en zona de exclusi�n
	if ((*env)->MonitorEnter(env, fftw_monitor) < 0) {
		(*env)->FatalError(env, "Error al tratar de ganar acceso exclusivo al objeto monitor");
	}
	plan = fftwf_plan_dft_c2r_1d(length,(fftwf_complex*)inptr,outptr,
			      (unsigned int)flag);
	(*env)->MonitorExit(env, fftw_monitor);


  fftwf_execute(plan);                       // Do the FFT
  
  // ENtra en zona de exclusi�n
  if ((*env)->MonitorEnter(env, fftw_monitor) < 0) {
	(*env)->FatalError(env, "Error al tratar de ganar acceso exclusivo al objeto monitor");
  }
  fftwf_destroy_plan(plan);                  // Clear up
  (*env)->MonitorExit(env, fftw_monitor);
  

  ReleaseFloatArray(in,inptr);             // Release the arrays
  ReleaseFloatArray(out,outptr);
}


/*                            twoDimensionaForward
 */
JNIEXPORT void JNICALL Java_jfftw_FFTWReal_nativeTwoDimensionalForwardF
(JNIEnv *env , jobject obj, jint width, jint height, jfloatArray in, jfloatArray out, jint flag){

	float *inptr, *outptr;                     // input and output array pointers  
	fftwf_plan  plan;                            // the plan
	
	inptr = GetFloatArray(in);                // Get input array
	outptr = GetFloatArray(out);              // get output array

	// Creates globlal reference to the monitor that controls exclusive access
	// to plan creation function
	if (fftw_monitor == NULL) {

		// Verifica la referencia
		if (FFTWReal_class == NULL) {
			FFTWReal_class = get_class_reference_WG(env, FFTW_REAL);
		}

		// Obtiene la referencia al monitor
		fftw_monitor = get_static_monitor_WG(env, FFTWReal_class, FFTW_MONITOR);
	}

	// ENtra en zona de exclusi�n
	if ((*env)->MonitorEnter(env, fftw_monitor) < 0) {
		char buffer[256];
		const char* error = "get_static_monitor_WG(): Error al obtener la referencia del objeto monitor %s";
		snprintf(buffer, strlen(FFTW_MONITOR) + strlen(error), error, FFTW_MONITOR);
		(*env)->FatalError(env, buffer);
	}
	// Make the two-d plan
	plan = fftwf_plan_dft_r2c_2d(height,width,inptr,
					(fftwf_complex*)outptr,
					(unsigned int)flag);

	(*env)->MonitorExit(env, fftw_monitor);
  
	fftwf_execute(plan);                       // Do the FFT

	// ENtra en zona de exclusi�n
	if ((*env)->MonitorEnter(env, fftw_monitor) < 0) {
		(*env)->FatalError(env, "Error al tratar de ganar acceso exclusivo al objeto monitor");
	}
	fftwf_destroy_plan(plan);                  // Clear up
	(*env)->MonitorExit(env, fftw_monitor);
  
	ReleaseFloatArray(in,inptr);             // Release the arrays
	ReleaseFloatArray(out,outptr);
}


/*                            twoDimensionaBackward
 */
JNIEXPORT void JNICALL Java_jfftw_FFTWReal_nativeTwoDimensionalBackwardF
(JNIEnv *env , jobject obj, jint width, jint height, jfloatArray in, jfloatArray out, jint flag){

  
  float *inptr, *outptr;                     // input and output array pointers  
  fftwf_plan  plan;                            // the plan

  inptr = GetFloatArray(in);                // Get input array
  outptr = GetFloatArray(out);              // get output array

	// Creates globlal reference to the monitor that controls exclusive access
	// to plan creation function
	if (fftw_monitor == NULL) {
		// Verifica la referencia
		if (FFTWReal_class == NULL) {
			FFTWReal_class = get_class_reference_WG(env, FFTW_REAL);
		}
		
		// Obtiene la referencia al monitor
		fftw_monitor = get_static_monitor_WG(env, FFTWReal_class, FFTW_MONITOR);
	}

	// Verifica el monitor
	if (fftw_monitor == NULL) {
		char buffer[256];
		const char* error = "get_static_monitor_WG(): Error al obtener la referencia del objeto monitor %s";
		snprintf(buffer, strlen(FFTW_MONITOR) + strlen(error), error, FFTW_MONITOR);
		(*env)->FatalError(env, buffer);
	}

	// ENtra en zona de exclusi�n
	if ((*env)->MonitorEnter(env, fftw_monitor) < 0) {
		(*env)->FatalError(env, "Error al tratar de ganar acceso exclusivo al objeto monitor");
	}
	plan = fftwf_plan_dft_c2r_2d(height,width,(fftwf_complex*)inptr,
			      outptr,(unsigned int)flag);
	(*env)->MonitorExit(env, fftw_monitor);

  
	fftwf_execute(plan);                       // Do the FFT


	// ENtra en zona de exclusi�n
	if ((*env)->MonitorEnter(env, fftw_monitor) < 0) {
		(*env)->FatalError(env, "Error al tratar de ganar acceso exclusivo al objeto monitor");
	}
	fftwf_destroy_plan(plan);                  // Clear up
	(*env)->MonitorExit(env, fftw_monitor);
  
	
	ReleaseFloatArray(in,inptr);             // Release the arrays
	ReleaseFloatArray(out,outptr);
}


/*                         threeDimensionalForward
 */
JNIEXPORT void JNICALL Java_jfftw_FFTWReal_nativeThreeDimensionalForwardF
(JNIEnv *env, jobject obj, jint width, jint height, jint depth, 
 jfloatArray in, jfloatArray out, jint flag) {

	float *inptr, *outptr;                    // input and output array pointers
	fftwf_plan plan;                            // the plan
	
	inptr = GetFloatArray(in);                // Get input array
	outptr = GetFloatArray(out);              // get output array

	// Creates globlal reference to the monitor that controls exclusive access
	// to plan creation function
	if (fftw_monitor == NULL) {

		// Verifica la referencia
		if (FFTWReal_class == NULL) {
			FFTWReal_class = get_class_reference_WG(env, FFTW_REAL);
		}

		// Obtiene la referencia al monitor
		fftw_monitor = get_static_monitor_WG(env, FFTWReal_class, FFTW_MONITOR);
	}

	if (fftw_monitor == NULL) {
		char buffer[256];
		const char* error = "get_static_monitor_WG(): Error al obtener la referencia del objeto monitor %s";
		snprintf(buffer, strlen(FFTW_MONITOR) + strlen(error), error, FFTW_MONITOR);
		(*env)->FatalError(env, buffer);
	}

	// Entra en zona de exclusi�n
	if ((*env)->MonitorEnter(env, fftw_monitor) < 0) {
		(*env)->FatalError(env, "get_static_monitor_WG(): Error al tratar de ganar acceso exclusivo al objeto monitor");
	}
	//                    Make the three-d plan
	plan = fftwf_plan_dft_r2c_3d(depth,height,width,inptr,
			      (fftwf_complex*)outptr,
			      (unsigned int)flag);
	(*env)->MonitorExit(env, fftw_monitor);
  
  
	fftwf_execute(plan);                       // Do the FFT

	if ((*env)->MonitorEnter(env, fftw_monitor) < 0) {
		(*env)->FatalError(env, "Error al tratar de ganar acceso exclusivo al objeto monitor");
	}
	fftwf_destroy_plan(plan);                  // Clear up
	(*env)->MonitorExit(env, fftw_monitor);


	ReleaseFloatArray(in,inptr);             // Release the arrays
	ReleaseFloatArray(out,outptr);
}

/*                            threeDimensionaBackward
 */
JNIEXPORT void JNICALL Java_jfftw_FFTWReal_nativeThreeDimensionalBackwardF
(JNIEnv *env , jobject obj, jint width, jint height, jint depth, jfloatArray in, jfloatArray out, jint flag){
  
  float *inptr, *outptr;                     // input and output array pointers  
  fftwf_plan  plan;                            // the plan
  
  inptr = GetFloatArray(in);                // Get input array
  outptr = GetFloatArray(out);              // get output array

  
  	// Creates globlal reference to the monitor that controls exclusive access
	// to plan creation function
	if (fftw_monitor == NULL) {

		// Verifica la referencia
		if (FFTWReal_class == NULL) {
			FFTWReal_class = get_class_reference_WG(env, FFTW_REAL);
		}

		fftw_monitor = get_static_monitor_WG(env, FFTWReal_class, FFTW_MONITOR);
	}

	if (fftw_monitor == NULL) {
		char buffer[256];
		const char* error = "get_static_monitor_WG(): Error al obtener la referencia del objeto monitor %s";
		snprintf(buffer, strlen(FFTW_MONITOR) + strlen(error), error, FFTW_MONITOR);
		(*env)->FatalError(env, buffer);
	}

	// Entra en zona de exclusi�n
	if ((*env)->MonitorEnter(env, fftw_monitor) < 0) {
		(*env)->FatalError(env, "Error al tratar de ganar acceso exclusivo al objeto monitor");
	}
	//                    Make the two-d plan
  plan = fftwf_plan_dft_c2r_3d(depth,height,width,(fftwf_complex*)inptr,
			      outptr,(unsigned int)flag);
  (*env)->MonitorExit(env, fftw_monitor);
  
  
  fftwf_execute(plan);                       // Do the FFT
  
  if ((*env)->MonitorEnter(env, fftw_monitor) < 0) {
		(*env)->FatalError(env, "Error al tratar de ganar acceso exclusivo al objeto monitor");
  }
  fftwf_destroy_plan(plan);                  // Clear 
  (*env)->MonitorExit(env, fftw_monitor);

  ReleaseFloatArray(in,inptr);             // Release the arrays
  ReleaseFloatArray(out,outptr);
}


