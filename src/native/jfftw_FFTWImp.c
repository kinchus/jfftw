/*          C implementation for FFT.java natives
 */
#include "jnimacros.h"
#include "jfftw_FFTW.h"
#include <fftw3.h>
#include <stdio.h>
#include <stdlib.h>


/*
 *     Author Will Hossack, 2008
 *
 *     This file is part of jfftw.
 *
 *    Jfftw is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    Jfftw is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with Jfftw.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


 JNIEXPORT void JNICALL 
 JNI_OnUnload(JavaVM *jvm, void *reserved)
 {
	 JNIEnv *env;

     if ((*jvm)->GetEnv(jvm, (void **)&env, JNI_VERSION_1_2)) {
         return;
     }

	 if (FFTWReal_class != NULL) {
		 (*env)->DeleteWeakGlobalRef(env, FFTWReal_class);
	 }
	if (FFTWComplex_class != NULL) {
		 (*env)->DeleteWeakGlobalRef(env, FFTWComplex_class);
	 }

     return;
 }


/*          nativeLoadWisdomFromFile method
 */
JNIEXPORT jboolean JNICALL Java_jfftw_FFTW_nativeLoadWisdomFromFile
(JNIEnv *env, jobject obj, jstring jfilename) {

  const char *filename;           // Filename in c format
  FILE *file;                     // The input file
  jboolean status = 0;            // status flag
  
  filename = GetString(jfilename);        // Get string in C format

  // printf("Wisdom file is %s\n",filename);
  
  file = fopen(filename,"r");             // Try and open file
  if (file != NULL) {                     // Try to read wisdom
    status = (jboolean)fftw_import_wisdom_from_file(file);
    fclose(file);
  }

  ReleaseString(jfilename,filename);      // release file
  return status;                          // Return status
  }

/*          nativeLoadWisdomFromString
 */
JNIEXPORT jboolean JNICALL Java_jfftw_FFTW_nativeLoadWisdomFromString
(JNIEnv *env, jobject obj, jstring jwisdom) {
  
  const char *wisdom = GetString(jwisdom);              // Get string in C format
  jboolean status  = (jboolean)fftw_import_wisdom_from_string(wisdom);
  ReleaseString(jwisdom,wisdom);
  return status;
}




/*          nativeClearWisdom method
 */
JNIEXPORT void JNICALL Java_jfftw_FFTW_nativeClearWisdom
(JNIEnv *env, jobject obj){
  fftw_forget_wisdom();                 // Simple void call
}


/*          nativeExportWisdomToFile method
 */

JNIEXPORT jboolean JNICALL Java_jfftw_FFTW_nativeExportWisdomToFile
(JNIEnv *env, jobject obj, jstring jfilename) {
  
  const char *filename;           // Filename in c format
  jboolean status = 0;            // status flag
  
  filename = GetString(jfilename);        // Get string in C format

  // printf("Wisdom output file is %s\n",filename);
  status =  fftw_export_wisdom_to_filename(filename);    // Do the write
  
  ReleaseString(jfilename,filename);      // release file
  return status;                          // Return status
  }


/*             nativeGetWisdom
 */

JNIEXPORT jstring JNICALL Java_jfftw_FFTW_nativeGetWisdom
(JNIEnv *env, jobject obj){
  
  char *wisdom = fftw_export_wisdom_to_string();   // Get the wisdom

  jstring jwisdom = NewString(wisdom);             // Make new java string
  fftw_free(wisdom );                              // Deallocate wisdom

  return jwisdom;                                  // return j staring
}



/*
 * Get monitor object by name as a weak global reference.
 * This function is not to be exported
 */
jobject get_static_monitor_WG(JNIEnv *env, jclass clazz, const char* monitor_name) {
	jobject obj = NULL;
	jobject ret = NULL;
	jfieldID fId;

	// Verifica la referencia
	if (clazz == NULL)
		(*env)->FatalError(env, "Error: get_static_monitor_WG(): la referencia de clase es NULL");

	// Obtiene la referencia al monitor
	fId = (*env)->GetStaticFieldID(env, clazz, monitor_name, "Ljava/lang/Object;");
	if (fId == NULL) {
		return (jobject)NULL;
	}
	
	obj = (*env)->GetStaticObjectField(env, clazz, fId);
	ret = (*env)->NewWeakGlobalRef(env, obj);

	return ret;
}


/*
 * Get class reference by name as a weak global reference.
 * This function is not to be exported
 */
jclass get_class_reference_WG(JNIEnv *env, const char* class_name)  {
	jclass ret = NULL;
	jclass cls = (*env)->FindClass(env, class_name);
     
	if ((cls != NULL)) {
         ret = (*env)->NewWeakGlobalRef(env, cls);
    }
	else {
		(*env)->FatalError(env, "Error: get_class_reference_WG(): No se pudo encontrar la clase.");
	}

	return ret;
}



  
