package jfftw;


/**        Class to forma Complex Engine for taking either
 *         forward of backward FFTs
 */

public class FFTWComplexEngine extends FFTWEngine {

    protected int sign;

    FFTWComplex fft = new FFTWComplex(this);


    /**       Form a three-dimensional ComplexEngine of specified size with
     *        specified input and output buffers.
     *        @param w the width
     *        @param h the height
     *        @param d the depth
     *        @param in the input buffer
     *        @param out the output buffer
     *        @param sign direction of the transform
     *        @throws NullPointerException if array null
     *        @throws IllegalArgumentException if array lengths
     *        do not match size.
     */
    public FFTWComplexEngine(int w, int h, int d,
				   double[] in, double[] out,
				   int sign) {
	//      Basic sanity checks !!!
	
	if (in == null || out == null ) {
	    throw new NullPointerException(
	   "FFTWComplexEngine: called with null data array.");
	}
	int size = 2*w*h*d;
	if (in.length != size || out.length != size) {
	    throw new IllegalArgumentException(
	    "FFTWComplexEngine: called with incompatible array lengths");
	}
	
	setDimensions(w,w,h,d);
	setInputBuffer(in);
	setOutputBuffer(out);
	setSign(sign);
    }

    /**       Form a two dimensional ComplexEngine of specified size with
     *        specified input and output buffers.
     *        @param w the width
     *        @param h the height
     *        @param in the input buffer
     *        @param out the output buffer
     *        @param sign direction of the transform
     *        @throws NullPointerException if array null
     *        @throws IllegalArgumentException if array lengths
     *        do not match size.
     */
    public FFTWComplexEngine(int w, int h, double[] in, double[] out,
				   int sign) {
	this(w,h,1,in,out,sign);
    }

    /**       Form a one dimensional ComplexEngine of specified size with
     *        specified input and output buffers.
     *        @param w the width
     *        @param in the input buffer
     *        @param out the output buffer
     *        @param sign direction of the transform
     *        @throws NullPointerException if array null
     *        @throws IllegalArgumentException if array lengths
     *        do not match size.
     */
    public FFTWComplexEngine(int w, double[] in, double[] out,
				   int sign) {
	this(w,1,1,in,out,sign);
    }
    


    /**       Form a three dimensional ComplexEngine of specified size
     *        The input and output buffers will be automatically
     *        created.
     *        @param w the width
     *        @param h the height
     *        @param d the depth
     *        @param sign direction of the transform
     *        @param overwrite if true then input and output buffer
     *        will be the same array, if false they will the seperate
     *        arrays.
     */  
    public FFTWComplexEngine(int w, int h, int d, int sign, 
			     boolean overwrite) {
	setDimensions(w,w,h,d);
	int size = 2*w*h*d;
	setInputBuffer(new double[size]);
	if (overwrite) {
	    setOutputBuffer(getInputBuffer());
	}
	else {
	    setOutputBuffer(new double[size]);
	}
	setSign(sign);
    }
    
    /**       Form a two-dimensional ComplexEngine of specified size
     *        The input and output buffers will be automatically
     *        created.
     *        @param w the width
     *        @param h the height
     *        @param sign direction of the transform
     *        @param overwrite if true then input and output buffer
     *        will be the same array, if false they will the seperate
     *        arrays.
     */  
    public FFTWComplexEngine(int w, int h, int sign, boolean overwrite) {
	this(w,h,1,sign,overwrite);
    }

    /**       Form a one-dimensional ComplexEngine of specified size
     *        The input and output buffers will be automatically
     *        created.
     *        @param w the width
     *        @param sign direction of the transform
     *        @param overwrite if true then input and output buffer
     *        will be the same array, if false they will the seperate
     *        arrays.
     */
    public FFTWComplexEngine(int w, int sign, boolean overwrite) {
	this(w,1,1,sign,overwrite);
    }


    /**          Method to set the sign to FORWARD of BACKWARD
     */
    public void setSign(int s) {
	sign = s;
    }


    /**         Method to get an input element as a Complex 
     *          @param i the element index
     *          @return <code>Complex</code> the element.
     */
    public Complex getInputComplex(int i) {
	return ArrayUtil.getComplex(inputBuffer,i);
    }

    /**         Method to get an output element as a Complex
     *          @param i the element index
     *          @return <code>Complex</code> the element
     */ 
    public Complex getOutputComplex(int i) {
	return ArrayUtil.getComplex(outputBuffer,i);
    }

    /**         Method to set the specifed element of the
     *          input complex with two doubles.
     *          @param i the elemnt index
     *          @param a the real value
     *          @param b the imaginary value
     */
    public void setInputComplex(int i, double a, double b) {
	ArrayUtil.setComplex(inputBuffer,i,a,b);
    }

    /**         Method to set the specifed element of the
     *          output complex with two doubles.
     *          @param i the elemnt index
     *          @param a the real value
     *          @param b the imaginary value
     */
    public void setOutputComplex(int i, double a, double b) {
	ArrayUtil.setComplex(outputBuffer,i,a,b);
    }


    public void update() {
	if (depth == 1 && height == 1) {
	    fft.oneDimensional(inputBuffer,outputBuffer,sign);
	}
	else if (depth == 1) {
	    fft.twoDimensional(inputWidth,height,inputBuffer,outputBuffer,sign);
	}
	else {
	    fft.threeDimensional(inputWidth,height,depth,inputBuffer,outputBuffer,
				 sign);
	}
    }
	


}



	    
