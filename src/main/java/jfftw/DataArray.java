package jfftw;
import java.util.Arrays;
import java.io.PrintStream;

/**      Abstract Class type to manipulate hold data for FFTW methods.
 *       Internally the data is held as an interleaved 
 *       <code>double[]</code> array as used by the FFTW library
 *       by this class take care of most of the
 *       array indexing and access problems via its supplied methods. 
 *       This is however at the cost of some efficiency.
 *       <p>
 *       Users wanting to take simple one-off FFT should use this
 *       classes, and the extending classes <code>RealDataArray</code> 
 *       and <code>ComplexDataArray</code>
 *       rather than the lower level <code>FFTW</code> classes.
 *
 *       @author Will Hossack, 2008
 *
 *
 *    This file is part of jfftw.
 *
 *    Jfftw is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    Jfftw is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with Jfftw.  If not, see <http://www.gnu.org/licenses/>.
 *
 *
 */
public abstract class DataArray extends Object implements Cloneable {

    /**    Static FFTW object used for taking the ffts, this is loaded
     *     statically once and pass shared to all instances.
     */
    protected static FFTW fftw = new FFTW();

    /**    PrintStream on which errors are printed. Initially defaults
     *    to <code>System.err</code>
     */
    protected PrintStream errorStream = System.err;

    /**    Internal data buffer holding the data in double[] array
     */
    protected double[] dataBuffer;


    /**    Flag for Complex to double conversion, initially defaults
     *     to <code>Complex.MODULUS</code>
     */
    protected int conversionFlag = Complex.MODULUS;

    /**    Width or primary size of the data array.
     */
    protected int width;

    /**    Current width span of data, this will alter under FFTs of real
     *     data.
     */
    protected int currentWidth;

    /**     Height or secondary size of data array.
     */
    protected int height;

    /**     Depth or third size of the data array
     */
    protected int depth;

    /**     Space flag initially defaults to <code>FFTW.REAL</code>
     */
    protected int space;
    
    private boolean normalise = true;

    /**      Method to get the length of the internal 
     *       <code>double[]</code> dataBuffer
     *       @return <code>int</code> length of dataBuffer
     */
    public int length() {
	return dataBuffer.length;
    }

    /**     Method to get the type (actually the class name) as
     *      a String.
     *      @return <code>String</code> type
     */
    public String getType() {
	String s =  getClass().getName();
        return s.substring(s.lastIndexOf('.') + 1);
    }


    /**     Method to get the current internal dataBuffer as a double[].
     *      <p>
     *      Take great care using this with <code>RealDataArray</code> 
     *      since the internal dataBuffer is re-located by its 
     *      <code>fourier()</code> method.
     *
     *      @return <code>double[]</code> the internal dataBuffer
     *      @see RealDataArray
     *      @see #fourier
     */
    public double[] getDataBuffer() {
	return dataBuffer;
    }

    /**     Method to set the internal dataBuffer to specified
     *      <code>double[]</code> array. 
     *      <p>
     *      Warning there is no internal sanity checking here.
     *      Really check you know what you are doing before using this. 
     *      If you are programming at this level your really should be using 
     *      the direct FFTW classes!
     *      @param db the dataBuffer
     */
    public void setDataBuffer(double[] db) {
	dataBuffer = db;
    }

    /**     Method to set the space. Initially defaults to 
     *      <code>FFTW.REAL</code>. 
     *      <p>
     *      If using <code>RealDataArray</code> class take extreme care
     *      using this; remember it does not change the data format, just the
     *      space designation flag.
     *      @param s <code>FFTW.REAL</code> or <code>FFTW.FOURIER</code>
     */
    public void setSpace(int s) {
	space = s;
    }

    /**    Method to set the sizes, not normally called directly since
     *     there is no interval sanity checking.
     *     <p>
     *     Not normally called by users, use with extreme care. If sizes
     *     do not match the data buffer you will get rubbish or will
     *     crash your JRE, you have been warned! 
     *
     *     @param w the width
     *     @param h the height
     *     @param d the depth
     */
    public void setDimensions(int w, int h, int d) {
	width = w;
	currentWidth = width;
	height = h;
	depth = d;
    }

    /**    Method to get the current space description flag.
     *     @return <code>int</code>Space code, either <code>FFTW.REAL</code>
     *           or <code>FFTW.FOURIER</code>
     */
    public int getSpace() {
	return space;
    }

    /**    Method to get the data width (primary direction).
     *     @return <code>int</code> the width
     *     @see #getCurrentWidth()
     */
    public int getWidth() {
	return width;
    }

    /**    Method to get the current width data width.
     *     Only different from getWidth() when in real Hermition
     *     Complex form when using <code>RealDataArray</code> objects.
     *     @return <code>int</code> the current width.
     *     @see #getWidth()
     */
    public int getCurrentWidth() {
	return currentWidth;
    }


    /**    Method to get the data height (secondary direction). This
     *     will be 1 for one-dimensional data. 
     *     @return <code>int</code> the height
     */
    public int getHeight() {
	return height;
    }

    /**    Method to get the data depth (tertiary direction). This will be
     *     1 for one or two-dimensional data.
     *     @return <code>int</code> the depth
     */
    public int getDepth() {
	return depth;
    }

    /**    Method to get the current Complex to double conversion flag.
     *     @return <code>int</code> conversion flag
     */
    public int getConversion() {
	return conversionFlag;
    }

    /**    Method to set default conversion from Complex to
     *     double. Default is <code>Complex.MODULUS</code>
     *     @param flag the conversion flag.
     */
    public void setConversion(int flag) {
	conversionFlag = flag;
    }    

    /**    Method to control auto normalisation on inverse flag,
     *     which is by default set to <code>true</code>.
     *     @param b the flag
     */
    public void setNormalisation(boolean b) {
	normalise = b;
    }
    
    /**     Method to get the normalisation flag.
     *      @return <code>boolean</code> the normalisation flag
     */
    public boolean getNormalisation() {
	return normalise;
    }

    /**    Method to determine if the current data is Complex.
     *     Data will be complex unless it is a <code>RealDataArray</code>
     *     in real space.
     *     @return <code>boolean</code> true if data is Complex
     */
    public boolean isComplex() {
	if (this instanceof RealDataArray && space == FFTW.REAL) {
	    return false;      // RealDataArray in real is real
	}
	else {
	    return true;       // All others are complex
	}
    }

    /**    Method to return the underlying FFTW class. 
     *     <p>
     *     Warning the FFTW class is loaded as static so changes
     *     to this will affect all instances.
     */
    public FFTW getFFTW() {
	return fftw;
    }

    /**   Default toString method to report information about 
     *    <code>DataArray</code>. This
     *    includes type, width, height, depth and current space.
     *    @return <code>String</code> information String.
     */
    public String toString() {
	return String.format("%s: w: %d h: %d d: %d s: %s",
			     getType(),width,height,depth,
			     (space == FFTW.REAL)?"Real":"Fourier");
    }


    /**   Method to get value of double element in one-dimension.
     *    If data is Complex, then the current Complex to double conversion
     *    rule is applied.
     *    @param i the element index
     */
    public abstract double getDouble(int i);

    /**   Method to get value of double element in two-dimensions.
     *    @param i primary index
     *    @param j secondary index
     *    @throws <code>ArrayIndexOutOfBoundsException</code> exception if
     *      out of data bounds.
     */
    public double getDouble(int i, int j) {
	boundsTest(i,j,0);
	return getDouble(j*currentWidth + i);
    }

    
    /**   Method to get value of double element in three-dimensions
     *    @param i primary index
     *    @param j secondary index
     *    @param k third index
     *    @throws <code>ArrayIndexOutOfBoundsException</code> exception if
     *      out of data bounds.
     */
    public double getDouble(int i, int j, int k) {
	boundsTest(i,j,k);
	return getDouble(currentWidth*(k*height + j) + i);
    }

    /**    Method to get the value of Complex element in one-dimensions case.
     *     If the data is real then the values will be returned in the
     *     real part of the Complex, with the imaginary set to zero.
     *     @param i the element index
     */
    public abstract Complex getComplex(int i); 

    /**    Method to get the value of a Complex element in two-dimensions.
     *     @param i primary index
     *     @param j secondary index
     *     @throws <code>ArrayIndexOutOfBoundsException</code> exception if
     *      out of data bounds.
     */
    public Complex getComplex(int i, int j) {
	boundsTest(i,j,0);
	return getComplex(j*currentWidth + i);
    }

    /**    Method to get the value of a Complex element in three-dimensions
     *     @param i primary index
     *     @param j secondary index
     *     @param k third index
     *     @throws <code>ArrayIndexOutOfBoundsException</code> exception if
     *      out of data bounds.
     */
    public Complex getComplex(int i, int j, int k) {
	boundsTest(i,j,k);
	return getComplex(currentWidth*(k*height + j) + i);
    }
    
    /**    Method to clear the databuffer by setting all elements
     *     to zero. Works for real and complex cases.
     */
    public void clear() {
	Arrays.fill(dataBuffer,0.0);
    }

    /**    Method to set element with a double in one-dimension.
     *     In the data is complex the Real part will be set.
     *     @param i the element index
     *     @param value the value
     */
    public abstract void setDouble(int i, double value);


    /**    Method to set element with a double in two-dimensions.
     *     @param i primary index
     *     @param j secondary index
     *     @param value the value
     *     @throws <code>ArrayIndexOutOfBoundsException</code> exception if
     *      out of data bounds.
     *     
     */
    public void setDouble(int i, int j, double value) {
	boundsTest(i,j,0);
	setDouble(j*currentWidth + i, value);
    }
    
    /**    Method to set element with a double in three-dimensions.
     *     @param i primary index
     *     @param j secondary index
     *     @param k third index
     *     @param value the value
     *     @throws <code>ArrayIndexOutOfBoundsException</code> exception if
     *      out of data bounds.
     */
    public void setDouble(int i, int j, int k, double value) {
	boundsTest(i,j,k);
	setDouble(currentWidth*(k*height + j) + i, value);
    }
    
    /**    Method to set an element with a complex specified as two
     *     doubles in one-dimension.
     *     @param i the element index
     *     @param a the real part 
     *     @param b the imaginary part
     */
    public abstract void setComplex(int i, double a, double b);

    /**    Method to set an element with a <code>Complex</code> in one
     *     dimensions.
     *     @param i the element
     *     @param c the Complex value
     */
    public void setComplex(int i, Complex c) {
	setComplex(i,c.r,c.i);
    }

    /**    Method to set an element with a complex in two-dimensions
     *     @param i primary index
     *     @param j secondary index
     *     @param a the real part 
     *     @param b the imaginary part
     *     @throws <code>ArrayIndexOutOfBoundsException</code> exception if
     *      out of data bounds.   
     */
    public void setComplex(int i, int j, double a, double b) {
	boundsTest(i,j,0);
	setComplex(j*currentWidth + i, a , b);
    }

    /**    Method to set an element with a <code>Complex</code> in two-dimensions.
     *     @param i primary index
     *     @param j secondary index
     *     @param c the Complex value
     *     @throws <code>ArrayIndexOutOfBoundsException</code> exception if
     *       out of data bounds.
     */
    public void setComplex(int i, int j, Complex c) {
	setComplex(i, j, c.r, c.i);
    }

    /**    Method to set an element with a Complex point in three-dimension.
     *     @param i primary index
     *     @param j secondary index
     *     @param k third index
     *     @param a the real part 
     *     @param b the imaginary part
     *     @throws <code>ArrayIndexOutOfBoundsException</code> exception if
     *       out of data bounds.
     */
    public void setComplex(int i, int j, int k, double a, double b) {
	boundsTest(i,j,k);
	setComplex(currentWidth*(k*height + j) + i, a, b);
    }

    /**    Method to set an element with a  <code>Complex</code> in 
     *     three-dimensions.
     *     @param i primary index
     *     @param j secondary index
     *     @param k third index
     *     @param c the Complex value
     *     @throws <code>ArrayIndexOutOfBoundsException</code> exception if
     *       out of data bounds.
     */
    public void setComplex(int i, int j, int k, Complex c) {
	setComplex(i, j, k ,c.r, c.i);
    }

    /**    Method to add a real <code>double</code> to an element in 
     *     one-dimensions. 
     *     @param i the element index
     *     @param a to be added
     */
    public abstract void add(int i, double a);

    /**    Method to multiple an element by a <code>double</code> in 
     *     one-dimensions.
     *     @param i the element index
     *     @param a the multiplier
     */
    public abstract void mult(int i, double a);

    /**    Method to add a real <code>double</code> to an element in 
     *     two-dimensions. 
     *     @param i the primary index
     *     @param j the secondary index
     *     @param a to be added 
     *     @throws <code>ArrayIndexOutOfBoundsException</code> exception if
     *       out of data bounds.
     *
     */
    public void add(int i, int j, double a) {
	boundsTest(i,j,0);
	add(j*currentWidth + i, a);
    }


    /**    Method to multiple an element by a <code>double</code> in 
     *     two-dimensions.
     *     @param i the primary index
     *     @param j the secondary index
     *     @param a the multiplier
     *     @throws <code>ArrayIndexOutOfBoundsException</code> exception if
     *       out of data bounds.
     */
    public void mult(int i, int j, double a) {
	boundsTest(i,j,0);
	mult(j*currentWidth + i, a);
    }

    
    /**    Method to add a real <code>double</code> to an element in 
     *     three-dimensions.
     *     @param i the primary index
     *     @param j the secondary index
     *     @param k the third index
     *     @param a to be added
     *     @throws <code>ArrayIndexOutOfBoundsException</code> exception if
     *       out of data bounds.
     */
    public void add(int i, int j, int k, double a) {
	boundsTest(i,j,k);
	add(currentWidth*(k*height + j) + i, a);
    }

    /**    Method to multiple an element by a <code>double</code> in 
     *     three-dimensions.
     *     @param i the primary index
     *     @param j the secondary index
     *     @param k the third index
     *     @param a the multiplier
     *     @throws <code>ArrayIndexOutOfBoundsException</code> exception if
     *       out of data bounds.
     */
    public void mult(int i, int j, int k, double a) {
	boundsTest(i,j,k);
	mult(currentWidth*(k*height + j) + i, a);
    }

    /**    Method to add a complex to an element in one-dimensions.
     *     @param i the element index
     *     @param a the real part
     *     @param b the imaginary parts
     */
    public abstract void  add(int i, double a, double b);

    /**    Method to multiply an element by a Complex  in one-dimension.
     *     @param i the element index
     *     @param a the real part
     *     @param b the imaginary parts
     */
    public abstract void mult(int i, double a, double b);

    /**    Method to add a <code>Complex</code> to an element in 
     *     one-dimensions.
     *     @param i the element index
     *     @param c to be added
     */
    public void add(int i, Complex c) {
	add(i,c.r,c.i);
    }

    /**    Method to multiply an element by a <code>Complex</code> in 
     *     one-dimension.
     *     @param i the element index
     *     @param c the multiplier
     */
    public void mult(int i, Complex c) {
	mult(i,c.r,c.i);
    }
    
    /**    Method to add a <code>Complex</code> to an element in 
     *     two-dimensions.
     *     @param i the primary index
     *     @param j the secondary index
     *     @param c to be added
     */
    public void add(int i, int j, Complex c) {
	boundsTest(i,j,0);
	add(j*currentWidth + i, c);
    }


    /**    Method to multiply an element by a <code>Complex</code> 
     *     in two-dimension.
     *     @param i the primary index
     *     @param j the secondary index
     *     @param c the multiplier
     */
    public void mult(int i, int j, Complex c) {
	boundsTest(i,j,0);
	mult(j*currentWidth + i, c);
    }
    
    /**    Method to add a <code>Complex</code> to an element in 
     *     three-dimensions.
     *     @param i the primary index
     *     @param j the secondary index
     *     @param k the third index
     *     @param c to be added
     */
    public void add(int i, int j, int k, Complex c) {
	boundsTest(i,j,k);
	add(currentWidth*(k*height + j) + i, c);
    }

    /**    Method to multiply an element by a <code>Complex</code> in 
     *     three-dimension.
     *     @param i the primary index
     *     @param j the secondary index
     *     @param k the third index
     *     @param c the multiplier
     */
    public void mult(int i, int j, int k, Complex c) {
	boundsTest(i,j,k);
	mult(currentWidth*(k*height + j) + i, c);
    }
    

    /**     Method to muliply the whole DataArray by a scalar
     *      @param a the scalar
     */
    public void mult(double a) {
	ArrayUtil.mult(dataBuffer,a);      // Use Array util (always valid)
    }


    /**    Method to multiply the whole DataArray by a complex specifed
     *     as two doubles. This is only valid if the data is Complex.
     *     @param a the real part
     *     @param b the imaginary.
     */
    public void mult(double a, double b) {
	if (isComplex()) {                   // Complex array
	    ArrayUtil.mult(dataBuffer,a,b);  // Use Array util
	}
	else {
	    errorStream.println("DataArray.mult complex used for real data");
	}
    }

   /**    Method to multiply the whole DataArray by a Complex.
     *    This is only valid if the data is Complex.
     *     @param c the Complex multiplier.
     */
    public void mult(Complex c) {
	mult(c.r,c.i);
    }

    /**    Method to form the Complex conjugate of an element in 
     *     one-dimensions.
     *     @param i the element index
     */
     public abstract void conjugate(int i);


    /**    Method to form the Complex conjugate of an element in 
     *     two-dimensions.
     *     @param i first element index
     *     @param j second element index
     *     @throws <code>ArrayIndexOutOfBoundsException</code> exception if
     *       out of data bounds.
     */
    public void conjugate(int i, int j) {
	boundsTest(i,j,0);
	conjugate(currentWidth*j + i);
    }

    /**    Method to form the Complex conjugate of an element in 
     *     three-dimensions.
     *     @param i first element index
     *     @param j second element index
     *     @param k the third index
     *     @throws <code>ArrayIndexOutOfBoundsException</code> exception if
     *       out of data bounds.
     */
    public void conjugate(int i, int j, int k) {
	boundsTest(i,j,k);
	conjugate(currentWidth*(k*height + j) + i);
    }

    /**   Method to take conjugate of the whole DataBuffer.
     *    If data is real this call is ignored.
     */
    public void conjugate() {
	if (isComplex()) {
	    ArrayUtil.conjugate(dataBuffer);  // Use Array util
	}
    }
    
        
    /**   Abstract method to form the un-shifted Fourier transform
     *  
     */
    public abstract void fourier();

    /**   Abstract method to apply a checker pattern
     *    to shift the centre
     */
    public abstract void checker();
    

    /**     Method to form centred Fourier using applying checker pattern 
     *      to shift DC the centre of the array.
     *      <p>
     *      This will only work for even dimensions, if called with data 
     *      of odd size, an error message will be printed and the 
     *      normal <code>fourier()</code> will be taken.
     */
    public void centreFourier() {
	
	if ((depth == 1 || depth%2 == 0) && (height == 1 || height%2 == 0) && 
	    width%2 == 0) {  // All even or 1, so OK    
	    checker();
	    fourier();
	    checker();
	}
	else {
	    errorStream.println("DataArray.centreFourier() called on data with odd size. Taking normal fourier()");
	    fourier();
	}
    }

    /**    Method to get the largest modulus square value in the DataBuffer
     *     This is valid for both real and complex data
     *     @return <code>double</code> the maximum modulus square
     */
    public double maxModSqr() {
	if (isComplex()) {
	    return ArrayUtil.maxModSqr(dataBuffer);
	}                             // Real array
	double max = 0.0;
	for(int i = 0; i < dataBuffer.length; i++) {
	    double x = dataBuffer[i];
	    x *= x;
	    if (x > max) max = x;
	}
	return max;
    }
	   

    /**   Method to get the total power in the DataSet being
     *    the sum of the modulus squared of all the elements
     *    @return <code>double</code> the power
     */
    public double power() {
	if (isComplex()) {          // Use Util static to do the work
	    return ArrayUtil.power(dataBuffer);
	}
	else {                      // Real, do summartion
	    double p = 0.0;
	    for(int i = 0; i < dataBuffer.length; i++) {
	    double x = dataBuffer[i];
	    p += x*x;
	    }
	    return p;
	}
    }


    /**   Get the current DataArray as a RealData array using the specified
     *    conversion flag.
     *    <p>
     *    If the current DataArray is <code>RealDataArray</code> in real space
     *    then the current DataArray is returned, else a new ReadDataArray
     *    of the correct dimensions is returned.
     *
     *    @param conversion the conversion flag
     *    @return <code>RealDataArray</code> the converted Real array
     */
    public RealDataArray getRealDataArray(int conversion) {

	//      Check if we have to do something ?

	if ( isComplex()){                   // Complex, so make RealDataArray
	    RealDataArray da = new RealDataArray(currentWidth,height,depth);

	    //      Get the lowlevel databuffer and fill it in one-dimensions
	    //      to save some of the index and getter overhead.
	    double d[] = da.getDataBuffer();
	    for(int i = 0; i < currentWidth*height*depth; i++) {
		d[i] = getComplex(i).getDouble(conversion);
	    }
	    return da;
	}
	else {                        
	    return (RealDataArray)this;        // Nothing to do !
	}
    }
	
    /**    Get the current DataArray are a RealDataArray using the internal
     *     conversion flag
     *     @return <code>RealDataArray</code> the converted Real array
     */
    public RealDataArray getRealDataArray() {
	return getRealDataArray(conversionFlag);
    }
    
    /**    Method to get the PowerSpectrum as a RealDataArray
     *     In data is in real space, then it is automatically
     *     Fourier transformed using <code>centreFourier</code>.
     *     <p>
     *     After this call the current <code>DataArray</code> will be in Fourier space.
     *
     *     @param logPower if true the log power formed, else modulus squared used.
     *     @return <code>RealDataArray</code> the power spectrum.
     */
    public RealDataArray powerSpectrum(boolean logPower) {
	
	//       In data in real space, take the FFT
	if (getSpace() == FFTW.REAL) centreFourier();
	
	//        Create an output RealDataArray of the correct size

	return getRealDataArray(logPower?Complex.LOG_POWER:Complex.MODULUS_SQUARED);
    }


    
    /**    Method to test if specified DataArray is of the same
     *     size at the current DataArray
     *     @return <code>boolean</code> true if match, false otherwise.
     */
    public boolean sizeMatch(DataArray d) {
	 return currentWidth == d.currentWidth &&
	     height == d.height && depth == d.depth ;
    }

    /**       Method to add a specified DataArray to the current DataArray, both must
     *        be the same size. 
     *        <p>
     *        This will work for all conbinations expect for current DataArray
     *        being Real and specifed DataArray being Complex.
     *        <p>
     *        Internally this uses direct array access so it much more
     *        efficient than loops with setter/getters.
     *        @param d the DataArray to be added.  
     */
    public void add(DataArray d) {
	if (sizeMatch(d)) {                              // Both same size
	    if (isComplex() ) {                          // Complex output
		if (d.isComplex()) {                     // Both complex
		    for(int i = 0; i < dataBuffer.length; i++) {
			dataBuffer[i] += d.dataBuffer[i];  // Add real /imag parts
		    }
		}
		else {                                   // real d, add real parts only     
		    for(int i = 0; i < d.dataBuffer.length; i++) {
			dataBuffer[2*i] += d.dataBuffer[i];
		    }
		}
	    }
	    else {                                         // Real output
		if (d.isComplex()) {
		    errorStream.println("DataBuffer.add: Real + Complex not supported");
		}
		else {                                     // Do real/real add
		    for(int i = 0; i < dataBuffer.length; i++) {
			dataBuffer[i] += d.dataBuffer[i];
		    }
		}
	    }
	}
    }
			
			

    /**       Method to multiply the current DataArray by a specified DataArray
     *        which must be of the same size, the result overwriting the contents
     *        of the current DataArray.
     *        <p>
     *        This will work for all conbinations expect for current DataArray
     *        being Real and specifed DataArray being Complex.
     *        <p>
     *        Internal this uses uses direct array access so it much more
     *        efficient than loops with setter/getters.
     *        @param d the DataArray 
     */
    public void mult(DataArray d) {
	if (sizeMatch(d)) {                                     // Both same size
	    if (isComplex() ) {                                 // Complex output
		if (d.isComplex()) {                            // Full Complex
		    ArrayUtil.mult(dataBuffer,d.dataBuffer);    // Use util method
		}
		else {                                           // d is real
		    for(int i = 0; i < currentWidth*height*depth; i++) {
			double da = d.dataBuffer[i];
			int ii = 2*i;
			dataBuffer[ii] *= da;             // Mult real part
			dataBuffer[ii + 1] *= da;         // Mult imaginary
		    }
		}
	    }
	    else {                                         // Real output
		if(d.isComplex()) { 	    
		    errorStream.println("DataBuffer.mult: Real x Complex not supported");
		}
		else {
		    //       Do direct multiply of the databuffers
		    for(int i = 0; i < dataBuffer.length; i++) {
			dataBuffer[i] *= d.dataBuffer[i];
		    }
		}
	    }
	}
	else {                                     // Wrong size
	    errorStream.println("DataBuffer.mult: Array sizes do not match.");
	}
    }

    /**       Method to multiply the current DataArray by the Complex
     *        Conjugate of specified DataArray which must be of the same size
     *        the result overwriting the contents of the current DataArray.
     *        <p>
     *        If the current or specifed DataArray(s) are real, then this method is identical
     *        to  <code>mult(DataArray)</code>
     *
     *        @param d the DataArray 
     *        @see #mult(DataArray)
     */
    public void multConjugate(DataArray d) {
	if (sizeMatch(d) ) {                            // Both same size
	    if (isComplex() && d.isComplex()) {         // Both complex, do conjugate mult
		ArrayUtil.multConjugate(dataBuffer,d.dataBuffer);
	    }
	    else {
		mult(d);                                // Do standard multiply
	    }
	}
	    
	else {                                          // Wrong size
	    errorStream.println("DataBuffer.multConjugate: Array sizes do not match.");
	}	
    }



    /**   Protected method to normalise the data buffer 
     */
    protected void normaliseDB() {
	double scale = 1.0/(width*height*depth);
	ArrayUtil.mult(dataBuffer,scale);
    }

    /**         Internal method to check array bounds. If outside
     *          bounds an ArrayIndexOutOfBoundsException is thrown
     *          and method does not return.
     */
    private void boundsTest(int i, int j, int k) {
	if (i >= currentWidth ) {
	    throw new ArrayIndexOutOfBoundsException(
                "DataArray: i index of: " + i +
		" but width of " + currentWidth);
	}
	else if (j >= height) {
	    throw new ArrayIndexOutOfBoundsException(
                "DataArray: j index of: " + j +
		" but width of " + height);
	}
	else if (k >= depth) {
	    throw new ArrayIndexOutOfBoundsException(
                "DataArray: k index of: " + k +
		" but width of " + depth);
	}
	else {
	    
	}
    }

}

    
    
