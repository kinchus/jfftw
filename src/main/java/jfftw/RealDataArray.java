package jfftw;
import java.awt.image.BufferedImage;
import java.awt.image.Raster;

/**      Class to handle Real Data arrays in either Real or
 *       Fourier space. Data in real space in held a double[]
 *       array on row order, but in Fourier space in Complex
 *       pairs with Hermition symmetry. For width of N the symmetry
 *       is given by F(k,m,n) = F*(N-k,m,n).
 *       <p>
 *       Note 1: in Fourier space the data width is given by
 *       <code>getFourierWidth()</code>
 *       <p>
 *       Note 2: The underlying FTTW is taking out-of-place
 *       transforms so the length and location of the internal
 *       databuffer WILL change when a transform is taken.
 *       <p>
 *       Warning to novice users; read and understand about
 *       FFTs before using this especially on two or three 
 *       dimensions.   
 *
 *       @author Will Hossack, 2008
 *
 *    This file is part of jfftw.
 *
 *    Jfftw is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    Jfftw is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with Jfftw.  If not, see <http://www.gnu.org/licenses/>.
 *
 *
 */

public class RealDataArray extends DataArray {

    private static FFTWReal fftwr = new FFTWReal(fftw);

    /**    Constructor to form a three-dimensional
     *     RealDataArray in real space.
     *     @param w the width (primary) dimension
     *     @param h the height (secondary) dimension
     *     @param d the depth (third) dimension
     */
    public RealDataArray(int w, int h, int d) {
	setDimensions(w,h,d);
	setSpace(FFTW.REAL);
	dataBuffer = new double[w*h*d];
    }

    /**    Constructor to form a two-dimensional
     *     RealDataArray, in real space.
     *     @param w the width (primary) dimension
     *     @param h the height (secondary) dimension
     */
    public RealDataArray(int w, int h) {
	this(w,h,1);
    }

    /**    Constructor to form a one-dimensional
     *     RealDataArray, in real space.
     *     @param w the width (primary) dimension
     */
    public RealDataArray(int w) {
	this(w,1,1);
    }

    /**    Form a RealDataArray with all parameters taken
     *     from the specified RealDataArray. The underlying
     *     data will also be copied.
     *     <p>
     *     This is valid in both real and Fourier space.
     *     @param data the RealDataArray
     */
    public RealDataArray(RealDataArray data) {
	setDimensions(data.getWidth(),
		      data.getHeight(),data.getDepth());
	setSpace(data.getSpace());
	dataBuffer = data.getDataBuffer().clone();
    }


    /**   Forms a general RealDataArray taking its data from the
     *    a supplied <code>double[]</code> assumed to be in the correct 
     *    (row) order.
     *    <p>
     *    Warning: when fourier() method is called the result will be 
     *    relocated to a new <code>double[]</code> array which can be 
     *    accessed by <code>getDataBuffer()</code>.
     *    Really understand what you are doing before playing with this!
     *
     *     @param w the width (primary) dimension
     *     @param h the height (secondary) dimension
     *     @param d the depth (third) dimension
     *     @param da the data array
     *     @throws <code>IllegalArgumentException</code> if length of
     *          data array does not match parameters.
     */
    public RealDataArray(int w, int h, int d, double[] da) {
	setDimensions(w,h,d);
	setSpace(FFTW.REAL);
	if (da.length != w*h*d) {     // Have been send junk
            throw new IllegalArgumentException( 
    "RealDataArray: length of data array does not match parameters");
        }
	dataBuffer = da;
    }

    /**    Constructor to form the a two-dimensional RealDataArray
     *     from a band of a Java <code>BufferedImage</code>.
     *     @param bi the BufferedImage supplying the size and data
     *     @param band the band to be used. If negative or greater that
     *     available bands, then the centre band will be used, so the
     *     zero band on a monochrome image, or band 1 of a tricoloured image.
     *     
     */     
    public RealDataArray(BufferedImage bi, int band) {
	this(bi.getWidth(),bi.getHeight());    // Make image array
	Raster raster = bi.getRaster();        // Get image raster
	int bands = raster.getNumBands();      // Actual number of bands
	if (band < 0 || band >= bands) {       // Check for legal
	    band = bands/2;                    // Default to centre
	}
	//       Copy the data from the raster in pixel by pixel basis
	for(int j = 0; j < height; j++) {
	    for(int i = 0; i < width; i++) {
		setDouble(i,j,raster.getSampleDouble(i,j,band));
	    }
	}
    }
				   
    

    /**    Form a deep clone of current RealDataArray, including data.
     *     @return <code>RealDataArray</code> cloned of current.
     */
    public RealDataArray clone() {
	return new RealDataArray(this);
    }
    
    /**   Method to get value of element as a double. If in real space is 
     *    just the element, but in Fourier, conversion from Complex to 
     *    real is controlled  by <code>setConversion()</code>, which defaults 
     *    to <code>Complex.MODULUS</code>.
     *    <p>
     *    For multi-dimensional array may be indexed as a one-dimenional
     *    array of length <code>width*height*depth</code>. Use this
     *    feature to bypass the array bound checking overhead.
     *    @param i the element index
     */
    public double getDouble(int i) {
	if (space == FFTW.REAL) {
	    return dataBuffer[i];
	}
	else {
	    if (conversionFlag == Complex.REAL) {       // Trap real
		return dataBuffer[2*i];
	    }
	    else if (conversionFlag == Complex.IMAG) {  // Trap imaginary
		return dataBuffer[2*i + 1];
	    }
	    else {                                     // Do hard way
		return getComplex(i).getDouble(conversionFlag);
	    }
	}
    }

    /**    Method to get a Complex in one-dimensional case. If in 
     *     Fourier space you get the expected element, while in real you 
     *     get the real element converted to a Complex with the imaginary 
     *     part set to zero.
     *     @param i the element index
     */
    public Complex getComplex(int i) {
	if (space == FFTW.FOURIER) {
	    return ArrayUtil.getComplex(dataBuffer,i);
	}
	else {
	    return new Complex(dataBuffer[i],0.0);
	}
    }

    /**    Method to set a double point in one-dimensions. 
     *     In real space does what in expected, and in Fourier set the 
     *     element to (a,0).
     *     @param i the element index
     *     @param a the value
     */
    public void setDouble(int i, double a) {
	if (space == FFTW.REAL) {
	    dataBuffer[i] = a;
	}
	else {
	    setComplex(i,a,0.0);
	}
    }

    /**    Method to set a Complex point in one-dimension with two doubles.
     *     If the data is in real space, then the complex is converted
     *     to a double using the current conversion rules.
     *     @param i the element index
     *     @param a the real part 
     *     @param b the imaginary part
     */
    public void setComplex(int i, double a, double b) {
	if (space == FFTW.FOURIER) {
	    i *= 2;
	    dataBuffer[i] = a;
	    dataBuffer[i + 1] = b;
	}
	else {
	    Complex c = new Complex(a,b);
	    dataBuffer[i] = c.getDouble(conversionFlag);
	}
    }


    /**    Multiply an element by a double, works in both real and 
     *     fourier space.
     *     @param i the element index
     *     @param a the multiplier
     */
    public void mult(int i, double a) {
	if (space == FFTW.REAL) {
	    dataBuffer[i] *= a;
	}
	else {
	    ArrayUtil.mult(dataBuffer,i,a);
	}
    }
    /**    Multiply an element by a Complex specified by two doubles in 
     *     Fourier space. In real space this will fail with printed 
     *     errors message.
     *     @param i the element index
     *     @param a the real part
     *     @param b the imaginary part
     */
    public void mult(int i, double a, double b) {
	if (space == FFTW.FOURIER) {
	    ArrayUtil.mult(dataBuffer,i,a,b);     // Use Util method
	}
	else {
	     errorStream.println("RealDataArray.mult complex used for real data");
	}
    }

    /**    Add a double to an element
     *     @param i the element index
     *     @param a to be added
     */
    public void add(int i, double a) {
	if (space == FFTW.REAL) {
	    dataBuffer[i] += a;
	}
	else {
	    i *= 2;
	    dataBuffer[i] += a;
	}
    }

    /**    Add a Complex specified by two doubles to an element
     *     @param i the element index
     *     @param a the real part
     *     @param b the imaginary part
     */
    public void add(int i, double a, double b) {
	if (space == FFTW.FOURIER) {
	    i *= 2;
	    dataBuffer[i] += a;
	    dataBuffer[i+1] += b;
	}
	else {
	     errorStream.println("RealDataArray.mult complex used for real data");
	}
    }
    
    
    /**      Form the conjugate of the ith Complex element. In in real space 
     *       it is ignored.
     *       @param i the element index
     */
    public void conjugate(int i) {
	if (space == FFTW.FOURIER) {
	    int ii = 2*i + 1;
	    dataBuffer[ii] = -dataBuffer[ii];
	}
    }

    /**    Method to get underlying FFTWReal
     *     @return <code>FFTWReal</code> used to do transforms.
     */
    public FFTWReal getFFTW() {
	return fftwr;
    }


    /**    Method to take in-place fft. If the space is REAL, then forward fft
     *     is taken, else inverse. 
     */
    public void fourier() {

	if (getSpace() == FFTW.REAL) {
	    setSpace(FFTW.FOURIER);
	    currentWidth = width/2 + 1;                 // Set current width to correct value.
	
	    //            Do forward fft (of the correct dimension)
	    if (depth > 1) {
		dataBuffer = 
		    fftwr.threeDimensionalForward(width,height,depth,dataBuffer);
	    }
	    else if (width > 1) {
		dataBuffer = fftwr.twoDimensionalForward(width,height,dataBuffer);
	    }
	    else {
		dataBuffer = fftwr.oneDimensionalForward(dataBuffer);
	    }
	}
	else {        //  Do backwards fft (of the correct dimension)
	    setSpace(FFTW.REAL);
	    currentWidth = width;                        // Set current width to match width
	    if (depth > 1) {
		dataBuffer = 
		    fftwr.threeDimensionalBackward(width,height,depth,dataBuffer);
	    }
	    else if (width > 1) {
		dataBuffer = fftwr.twoDimensionalBackward(width,height,dataBuffer);
	    }
	    else {
		dataBuffer = fftwr.oneDimensionalBackward(dataBuffer);
	    }
            //       Done back(inverse) FFT, so normalise if required
	     if (getNormalisation() ) {
		 normaliseDB();
	     }
	}
    }

    /**       Method to apply a checker in either Real or Fourier space.
     */
    public void checker() {
	if (space == FFTW.REAL) {       // Deal with real case
	    
	    for(int k = 0; k < depth; k++) {
		int kstart = k%2;
		// int planeStart = k*width*height;      // Star of plane
		for(int j = 0; j < height; j++) {
		    int jstart = (j + kstart)%2;                   // alternate 0/1 start
		    int lineStart = j*width;            // Start of the line
		    for(int i = jstart; i < width; i += 2) {
			int ii = lineStart + i;       // Pixel location
			dataBuffer[ii] = -dataBuffer[ii];
		    }
		}
	    }
	}
	else {
	    	for(int k = 0; k < depth; k++) {
		    int kstart = k%2;
		    // int planeStart = k*width*height;      // Star of plane
		    for(int j = 0; j < height; j++) {
			int jstart = (j + kstart)%2;                   // alternate 0/1 start
			int lineStart = j*currentWidth;            // Start of the line
			for(int i = jstart; i < currentWidth; i += 2) {
			    int realPart = 2*(lineStart + i); // Location on real
			    int imagPart = realPart + 1;      // Location of imaginary
			    dataBuffer[realPart] = -dataBuffer[realPart];       // Invert real
			    dataBuffer[imagPart] = -dataBuffer[imagPart];       // Invert imag
			}
		    }
		}
	}
    }

	    
	    
}
