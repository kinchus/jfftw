package jfftw;

/**     Class to implement a FFT Engine based on FFTW.
 *          
 */

public abstract class FFTWEngine extends FFTW {

    /**          Width of the input data
     */
    protected int inputWidth;

    /**          Width of the output data 
     */
    protected int outputWidth;

    /**           Height of the data
     */
    protected int height;

    /**            Depth of the data
     */
    protected int depth;

    /**       The input buffer of the Engine
     */
    protected double[] inputBuffer;

    /**       The output buffer of the Engine
     */
    protected double[] outputBuffer;


    /**     Method to set the input buffer to specifed array
     *      @param in the input buffer
     */
    public void setInputBuffer(double[] in) {
	inputBuffer = in;
    }

    /**     Method set the input buffer to new array of specified length
     *      @param size length of input buffer
     */
    public void setInputBuffer(int size) {
	inputBuffer = new double[size];
    }

    /**     Method to get the input buffer
     *      @return <code>double[]</code> the input buffer
     */
    public double[] getInputBuffer() {
	return inputBuffer;
    }

    /**     Method to set the output buffer to specifed array
     *      @param out the output buffer
     */
    public void setOutputBuffer(double[] out) {
	outputBuffer = out;
    }

    /**     Method set the output buffer to new array of specified length
     *      @param size length of output buffer
     */
    public void setOutputBuffer(int size) {
	outputBuffer = new double[size];
    }

    /**     Method to get the output buffer
     *      @return <code>double[]</code> the output buffer
     */
    public double[] getOutputBuffer() {
	return outputBuffer;
    }


    /**    Method to set the dimensions.
     *     <p>
     *     Note this ONLY sets the dimensions, no other checking!
     */
    public void setDimensions(int iw, int ow, int h, int d) {
	inputWidth = iw;
	outputWidth = ow;
	height = h;
	depth = d;
    }

    /**     Method to get the input array width.
     */
    public int getInputWidth() {
	return inputWidth;
    }

    /**     Method to get the output data width
     */
    public int getOutputWidth() {
	return outputWidth;
    }

    /**     Method to get the height
     */
    public int getHeight() {
	return height;
    }


    /**      Method to get the depth
     */
    public int getDepth() {
	return depth;
    }


    /**         Absract method to get the specifed input element
     *          as a Complex
     *          @param i the element index
     */
    public abstract Complex getInputComplex(int i); 


    /**         Method to get the spefied input elements as complex
     *          using two dimensional access.
     *          @param i the i index
     *          @param j the j index
     */
    public Complex getInputComplex(int i, int j) {
	return getInputComplex(j*inputWidth + i);
    }

    /**         Method to get the spefied input elements as complex
     *          using three dimensional access.
     *          @param i the i index
     *          @param j the j index
     *          @param k the k index
     */
    public Complex getInputComplex(int i, int j, int k) {
	return getInputComplex(inputWidth*(k*height + j) + i);
    } 
    /**         Absract method to get the specifed output element
     *          as a Complex
     *          @param i the element index
     */
    public abstract Complex getOutputComplex(int i); 


    /**         Method to get the spefied output element as a complex
     *          using two dimensional access.
     *          @param i the i index
     *          @param j the j index
     */
    public Complex getOutputComplex(int i, int j) {
	return getOutputComplex(j*inputWidth + i);
    }

    /**         Method to get the spefied output element as a complex
     *          using three dimensional access.
     *          @param i the i index
     *          @param j the j index
     *          @param k the k index
     */
    public Complex getOutputComplex(int i, int j, int k) {
	return getOutputComplex(inputWidth*(k*height + j) + i);
    } 

    /**      Abstact method to set an input input complex element
     *       with two doubles.
     *       @param i the element index
     *       @param a the real value
     *       @param b the imaginary value
     */ 
    public abstract void setInputComplex(int i, double a, double b);

    /**      Method to set an input input complex element
     *       with a complex
     *       @param i the element index
     *       @param c the Complex
     */ 
    public void setInputComplex(int i, Complex c) {
	setInputComplex(i,c.r,c.i);
    }
    
    /**      Method to set an input input complex element
     *       with a complex ueing two-diemnsional access
     *       @param i the i element index
     *       @param j the j element index
     *       @param c the Complex
     */ 
    public void setInputComplex(int i, int j, Complex c) {
	setInputComplex(j*inputWidth + i,c.r,c.i);
    }

    /**      Method to set an input input complex element
     *       with a complex ueing three-diemnsional access
     *       @param i the i element index
     *       @param j the j element index
     *       @param k the k element index
     *       @param c the Complex
     */
    public void setInputComplex(int i, int j, int k, Complex c) {
	setInputComplex(k*inputWidth*(height + j) + i,c.r,c.i);
    }

    /**      Abstact method to set an output complex element
     *       with two doubles.
     *       @param i the element index
     *       @param a the real value
     *       @param b the imaginary value
     */ 
    public abstract void setOutputComplex(int i, double a, double b);

    /**      Method to set an output complex element
     *       with a complex
     *       @param i the element index
     *       @param c the Complex
     */ 
    public void setOutputComplex(int i, Complex c) {
	setOutputComplex(i,c.r,c.i);
    }
    
    /**      Method to set an output complex element
     *       with a complex ueing two-diemnsional access
     *       @param i the i element index
     *       @param j the j element index
     *       @param c the Complex
     */ 
    public void setOutputComplex(int i, int j, Complex c) {
	setOutputComplex(j*inputWidth + i,c.r,c.i);
    }

    /**      Method to set an output complex element
     *       with a complex ueing three-diemnsional access
     *       @param i the i element index
     *       @param j the j element index
     *       @param k the k element index
     *       @param c the Complex
     */
    public void setOutputComplex(int i, int j, int k, Complex c) {
	setOutputComplex(k*inputWidth*(height + j) + i,c.r,c.i);
    }


    public String toString() {
	String s = getClass().getName();
	s = s.substring(s.lastIndexOf('.') + 1);
	return s + ": iw: " + getInputWidth() + " ow: " +
	    getOutputWidth() + " h: " + getHeight() + 
	    " d: " + getDepth();
    }

	
    

}