package jfftw;
import java.awt.image.BufferedImage;
import java.awt.image.Raster;

/**      Class to handle Complex Data arrays in either Real or
 *       Fourier space. This is the simplest fft interface
 *       with simple support for one, two and three dimensional
 *       complex data with, almost, all indexing problems covered
 *       by the support methods.
 *       <p>
 *       Novice users should start here, and use the more complex
 *       <code>RealDataArray</code> or the low-level <code>FFTW</code>
 *       if they want need either more efficient interface or have data
 *       formatted other than the interleaved complex implemented here.
 *       <p>
 *       @author Will Hossack, 2008
 *
 *    This file is part of jfftw.
 *
 *    Jfftw is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    Jfftw is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with Jfftw.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
public class ComplexDataArray extends DataArray {

    private static FFTWComplex fftwc = new FFTWComplex(fftw);

    /**    Constructor to form a three-dimensional
     *     ComplexDataArray, which defaults to <code>FFTW.REAL</code> space.
     *     <p>
     *     All data is pre-initialised to zero.
     *     @param w the width (primary) dimension
     *     @param h the height (secondary) dimension
     *     @param d the depth (third) dimension
     */
    public ComplexDataArray(int w, int h, int d) {
	setDimensions(w,h,d);
	setSpace(FFTW.REAL);
	dataBuffer = new double[2*w*h*d];
    }

    /**    Constructor to form a two-dimensional
     *     ComplexDataArray, which defaults to <code>FFTW.REAL</code> space.
     *     @param w the width (primary) dimension
     *     @param h the height (secondary) dimension
     */
    public ComplexDataArray(int w, int h) {
	this(w,h,1);
    }

    /**    Constructor to form a one-dimensional
     *     ComplexDataArray, which defaults to <code>FFTW.REAL</code> space.
     *     @param w the width (primary) dimension
     */
    public ComplexDataArray(int w) {
	this(w,1,1);
    }


    /**   Form a general ComplexDataArray with specified dimensional 
     *    and supplied data array, which is NOT copied, it is
     *    used internally.
     *    <p>
     *    Use with care, you must format the supplied data in the 
     *    correct row interleaved format. If you are using this
     *    in a main program, consider using the low-level level
     *    <code>FFTWComplex</code> class.
     * 
     *     @param w the width (primary) dimension
     *     @param h the height (secondary) dimension
     *     @param d the depth (third) dimension
     *     @param da the data array.
     *     @throws <code>IllegalArgumentException</code> if length of
     *                   data array does not match parameters.

     */
    public ComplexDataArray(int w, int h, int d, double[] da ) {
	setDimensions(w,h,d);
	setSpace(FFTW.REAL);
	if (da.length != 2*w*h*d) {     // Have been send junk
	    throw new IllegalArgumentException( 
	"ComplexDataArray: length of data array does not match parameters");

	}
	dataBuffer = da;
    }

	

    /**    Form a <code>ComplexDataArray</code> from supplied 
     *     <code>DataArray</code>. 
     *     <p>
     *     If supplied with <code>ComplexDataArray</code> it will produce a 
     *     clone including the data. 
     *     <p>
     *     For a <code>RealDataArray</code> the real values will be used
     *     to set the real parts with imaginary defaulting to zero. Note:
     *     this will only work for RealDataArray in real space.  

     *     @param data the specifying DataArray
     *     @throws <code>IllegalArgumentException</code> if
     *     real array is in Fourier space.
     */
    public ComplexDataArray(DataArray data) {
	
	 setDimensions(data.getWidth(),data.getHeight(),data.getDepth());
	 setSpace(data.getSpace());

	 if (data instanceof ComplexDataArray) {   // Just clone buffer
	     dataBuffer = data.getDataBuffer().clone();
	 }
	 else if (data instanceof RealDataArray) {
	     if (data.getSpace() == FFTW.REAL) {    // Real space only
		 dataBuffer = ArrayUtil.interleave(data.dataBuffer,null);
	     }
	     else {
		 throw new IllegalArgumentException( 
     "ComplexDataArray: called with RealDataArray in Fourier space.");
	     }
	 }
	 else {
	     throw new IllegalArgumentException( 
       "ComplexDataArray: called with unknown DataArray type.");
	     
	 }
    }
	    

    /**    Constructor to form the a two-dimensional ComplexDataArray
     *     from a band of a Java <code>BufferedImage</code>. The 
     *     real parts are set by the image data, with the imaginary parts
     *     set to zero. 
     *     @param bi the BufferedImage supplying the size and data
     *     @param band the band to be used. If negative or greater that
     *     available bands, then the centre band will be used, so the
     *     zero band on a monochrome image, or band 1 of a tricoloured image.
     *     
     */     
    public ComplexDataArray(BufferedImage bi, int band) {
	this(bi.getWidth(),bi.getHeight());    // Make image array
	Raster raster = bi.getRaster();        // Get image raster
	int bands = raster.getNumBands();      // Actual number of bands
	if (band < 0 || band >= bands) {       // Check for legal
	    band = bands/2;                    // Default to centre
	}
	//       Copy the data from the raster in pixel by pixel basis
	for(int j = 0; j < height; j++) {
	    for(int i = 0; i < width; i++) {
		setComplex(i,j,raster.getSampleDouble(i,j,band),0.0);
	    }
	}
    }
    
    /**    Form a deep clone of current ComplexDataArray, including data.
     *     @return <code>ComplexDataArray</code> cloned of current.
     */
    public ComplexDataArray clone() {
	return new ComplexDataArray(this);
    }
    
    /**    Method to get a Complex element in one-dimensional case. 
     *     This implements the abstract methods defined 
     *     in <code>DataArray</code> and handles the internal indexing.
     *     <p>
     *     For a multi-dimensional array this can be treated as 
     *     a one-dimensional array length 
     *     <code>width*height*depth</code> allowing more efficiency 
     *     one-dimensional access. This bypasses the array bound
     *     checking.
     *     @param i the element index
     */
    public Complex getComplex(int i) {
	return ArrayUtil.getComplex(dataBuffer,i);
    }

    /**    Method to get the double value of the element on one-dimensions
     *     where the Complex real conversion is controlled by 
     *     <code>setConversion()</code>, which defaults to 
     *     <code>Complex.MODULUS</code>.
     *     @param i the element index.
     */
    public double getDouble(int i) {
	if (conversionFlag == Complex.REAL) {    // Trap real/imag
	    return dataBuffer[2*i];
	} 
	else if (conversionFlag == Complex.IMAG) {
	    return dataBuffer[2*i + 1];
	}
	else {                                    // Do it the hard way....
	return getComplex(i).getDouble(conversionFlag);
	}
    }

    /**    Method to set a Complex point in one-dimension with two doubles.
     *     @param i the element index
     *     @param a the real part 
     *     @param b the imaginary part
     */
    public void setComplex(int i, double a, double b) {
	ArrayUtil.setComplex(dataBuffer,i,a,b);
    }

    /**   Method to set the Real part of an element with a double,
     *    the imaginary part is not modified.
     *    @param i the element index
     *    @param a the double value
     */
    public void setDouble(int i, double a) {
	dataBuffer[2*i] = a;
    }


    /**    Multiply an element by a double
     *     @param i the element index
     *     @param a the multiplier
     */
    public void mult(int i, double a) {
	ArrayUtil.mult(dataBuffer,i,a);
    }

    /**    Multiply an element by a Complex specified
     *     @param i the element index
     *     @param a the real part
     *     @param b the imaginary part
     */
    public void mult(int i, double a, double b) {
	ArrayUtil.mult(dataBuffer,i,a,b);    // Use Util method
    }


    /**    Add a double to real part of element
     *     @param i the element index
     *     @param a to be added to real part
     */
    public void add(int i, double a) {
	i *= 2;
	dataBuffer[i] += a;
    }

    /**    Add a Complex to specified
     *     @param i the element index
     *     @param a the real part
     *     @param b the imaginary part
     */
    public void add(int i, double a, double b) {
	i *= 2;
	dataBuffer[i] += a;
	dataBuffer[i+1] += b;
    }


    /**    Form the conjugate of the element on one-dimensions
     *     by negating the imaginary parts.
     *     @param i the element to be conjugated. 
     */
    public void conjugate(int i) {
	ArrayUtil.conjugate(dataBuffer,i);
    }
	

    /**    Method to get underlying FFTWComplex
     *     @return <code>FFTWComplex</code> used to do transforms.
     */
    public FFTWComplex getFFTW() {
	return fftwc;
    }


    /**    Method to take in-place fft. If the space is REAL, then forward fft
     *     is taken, else inverse. Normalisation by total data size will occur
     *     on the inverse transform provided that the set normalisation flag
     *     is set to <code>true</code>
     *     @see #setNormalisation 
     */
    public void fourier() {
	int direction = 0;

	//             Decide on direction and reset space
	if (getSpace() == FFTW.REAL) {
	    direction = 1;
	    setSpace(FFTW.FOURIER);
	}
	else {
	    direction = -1;
	    setSpace(FFTW.REAL);
	}
	
	//            Do in-place fft (of the correct dimension)
	if (depth > 1) {
	    dataBuffer = fftwc.threeDimensional(width,height,depth,
						dataBuffer,dataBuffer,
						direction);
	}
	else if (width > 1) {
	    dataBuffer = fftwc.twoDimensional(width,height,
					      dataBuffer,dataBuffer,
					      direction);
	}
	else {
	    dataBuffer = fftwc.oneDimensional(dataBuffer,dataBuffer,
					      direction);
	}
	    
	/*    If we have gone back to REAL space, and normalisation is
	 *    set, divide through by number of data points.
	 */
	if (getSpace() == FFTW.REAL && getNormalisation() ) normaliseDB();
    }



    /**     Method to apply a +/1 checker patterns to the Complex
     *      data, used to form centred FFTW.
     */
    public void checker() {
        	    
	for(int k = 0; k < depth; k++) {
	    int kstart = k%2;
	    //int planeStart = k*width*height;      // Start of plane
	    for(int j = 0; j < height; j++) {
		int jstart = (j + kstart)%2;                   // alternate 0/1 start
		int lineStart = j*width;            // Start of the line
		for(int i = jstart; i < width; i += 2) {
		    int realPart = 2*(lineStart + i); // Location on real
		    int imagPart = realPart + 1;      // Location of imaginary
		    dataBuffer[realPart] = -dataBuffer[realPart];       // Invert real
		    dataBuffer[imagPart] = -dataBuffer[imagPart];       // Invert imag
		}
	    }
	}
    }



}
