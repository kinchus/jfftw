package jfftw;
/**
 *         Class to handle Complex numbers and arithmetic.
 *         The complex is held a two double, and all arithmetic
 *         is in double format.
 *
 *         Much of the internal coding does not use methods for
 *         efficiency reasons.
 *
 *         @author Will Hossack, 2008
 *         @version 3.0
 *
 *    This file is part of jfftw.
 *
 *    Jfftw is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    Jfftw is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with Jfftw.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
public class ComplexF extends Object implements Cloneable {

    /**       Static to specify real part of Complex number
     */
    public static final int REAL = 0x1;

    /**       Static to specify imaginary part of Complex number
     */
    public static final int IMAG = 0x2;

    /**        Static to specify both parts of a Complex number
     */
    public static final int BOTH = 0x3;

    /**        Static to specify the modulus of a Complex number
     */
    public static final int MODULUS = 0x8;

    /**        Static to specify the phase of a Complex number
     */
    public static final int PHASE = 0x10;

       /**        Static to specify the modulus squared of a Complex number
     */
    public static final int MODULUS_SQUARED = 0x20;

    /**        Static to specify Log Power of a Complex
     */
    public static final int LOG_POWER = 0x40;


    /**           The real part
     */
    public float r;

    /**           The imaginary part
     */
    public float i;
    
    

    private static String fmt = "%g";

    /**   Constructor for Complex with two doubles
     *    @param a real part
     *    @param b imaginary part
     */
    public ComplexF(float a, float b) {
	set(a,b);
    }

    /**  Constructor for Complex with real part only
     *   @param a real part
     */
    public ComplexF(float a) {
	set(a,0.0F);
    }

    /**  Default constructor for Complex with both real and imaginary zero
     */
    public ComplexF() {
	set(0.0F,0.0F);
    }

    /**  Constructor for Complex with Complex parameter
     *   @param c the Complex value
     */
    public ComplexF(ComplexF c) {
	set(c);
    }


    /**   Method to clone the current Complex
     *    @return <code>Complex</code> clone of the current Complex 
     */
    public ComplexF clone() {
	return new ComplexF(this);
    }

    /**   Method reset Complex value 
     *    @param a the real part
     *    @param b the imaginary value
     */
    public void set(float a, float b) {
	r = a;
	i = b;
    }

    /**   Method to reset the Complex value
     *    @param c the Complex value
     */
    public void set(ComplexF c) {
	r = c.r;
	i = c.i;
    }

    /**   Method to set the Complex value with polar parameters
     *    @param rho the radial parameter
     *    @param theta the angular parameter
     */
    public void setPolar(float rho, float theta) {
	set((float)(rho*Math.cos(theta)),(float)(rho*Math.sin(theta)));
    }


    /**   Method to set Complex value to expi(theta)
     *   @param theta the angular parameter
     */
    public void setExpi(float theta) {
	setPolar(1.0F,theta);
    }


    /**   Method to set a Complex to a specified modulus, but
     *    the phase set randomly between 0 to 2pi
     *    @param m the modulus
     */
    public void setRandomPhase(float m){
	float theta = (float)(2.0*Math.PI*Math.random());
	setPolar(m,theta);
    }


    /**   Method to set the current Complex to be invalid,
     *    with both real and imaginary set to NaN
     */
    public void setInvalid() {
	set(Float.NaN,Float.NaN);
    }


    /**     Test of either components set to NaN
     *      @return <code>boolean</code> true if NaN]
     */
    public boolean isNaN() {
	if (Float.isNaN(r) || Float.isNaN(i))
	    return true;
	else
	    return false;
    }


    /**      Set the real part, imaginary part unchanged
     *       @param a the real value
     */
    public void setReal(float a) {
	r = a;
    }

    /**     Set the imaginary part, real part unchanged
     *      @param b the imaginary part
     */
    public void setImag(Float b) {
	i = b;
    }

    /**      Get the real part
     *       @return <code>double</code> the real part
     */
    public float getReal() {
	return r;
    }

    /**     Get the imag part
     *      @return <code>double</code> the imaginary part
     */
    public float getImag() {
	return i;
    }

    /**     Get the modulus squared of the Complex
     *      @return <code>double</code> the modulus squared
     */ 
    public float modulusSq() {
	return r*r + i*i;
    }

    /**    Get the modulus of the Complex
     *     @return <code>double</code> the modulus
     */
    public float modulus() {
	return (float)Math.sqrt(r*r + i*i);
    }

    /**    Get the phase from atan2(i,r) method. 
     *     @return <code>double</code> the phase in range -pi -> pi
     */
    public float phase() {
	return (float)Math.atan2(i,r);
    }



    /**    Get the log power, being defines at log(r*r + i*i + 1.0)
     *     @return <code>double</code> the log power
     */
    public float logPower() {
	return (float)Math.log(r*r + i+i + 1.0);
    }


    /**       Method to get double, converted from current Complex 
     *        as specified by the conversion flag.
     *        @param flag the conversion flag
     */
    public float getDouble(int flag) {
   
	/*                    Switch to return the right value
	 */
	switch (flag) {
	case REAL:
	    return r;

	case IMAG:
	    return i;
	
	case MODULUS:
	    return modulus();
	
	case MODULUS_SQUARED:
	    return modulusSq();

	case PHASE: 
	    return phase();
	
	case LOG_POWER:
	    return logPower();
	
	default :                   // Not value, so NaN
	    return Float.NaN;
	}
    }
    

    /**    Add a complex to the current returning  a new Complex.
     *     @param c the complex
     *     @return <code>Complex</code> the sum
     */
    public ComplexF plus(ComplexF c) {
	return new ComplexF(r + c.r,i + c.i);
    }

    /**    Add a complex to the current returning a new Complex 
     *     @param a the real part
     *     @param b the imaginary part
     *     @return <code>Complex</code> the sum
     */
    public ComplexF plus(float a, float b) {
	return new ComplexF(r + a, i + b);
    }

    /**    Add a real number to the current returning a new Complex.
     *     @param a the real part
     *     @return <code>Complex</code> the sum
     */ 
    public ComplexF plus(float a) {
	return new ComplexF(r + a, i);
    }

    /**    Subtract a complex from the current retuning a new Complex
     *     @param c the complex
     *     @return <code>Complex</code> the result
     */
    public ComplexF minus(ComplexF c) {
	return new ComplexF(r - c.r,i - c.i);
    }

    /**    Subtract a complex from the current returning a new Complex
     *     @param a the real part
     *     @param b the imaginary part
     *     @return <code>Complex</code> the result
     */
    public ComplexF minus(float a, float b) {
	return new ComplexF(r - a, i - b);
    }

    /**    Subtract a real number from the current retuning a new Complex
     *     @param a the real part
     *     @return <code>Complex</code> the result
     */ 
    public ComplexF minus(float a) {
	return new ComplexF(r - a, i);
    }


    /**    Subtract the current Complex FROM specified value returning
     *     a new Complex
     *     @param c the complex
     *     @return <code>Complex</code> the result
     */
    public ComplexF from(ComplexF c) {
	return new ComplexF(c.r - r,c.i - i);
    }

    /**    Subtract the current Complex FROM specified value retuning
     *     a new Complex
     *     @param a the real part
     *     @param b the imaginary part
     *     @return <code>Complex</code> the result
     */
    public ComplexF from(float a, float b) {
	return new ComplexF(a - r, b - i);
    }

    /**    Subtract the current Complex from a scalar returning a new
     *     Complex
     *     @param a the real part
     *     @return <code>Complex</code> the result
     */ 
    public ComplexF from(float a) {
	return new ComplexF(a - r, -i);
    }


    /**    Multiply the current by a scalar returning a new Complex
     *     @param a the scalar
     *     @return <code>Complex</code> the multiplication
     */
    public ComplexF mult(float a) {
	return new ComplexF(a*r , a*i);
    }

    /**    Multiply the current by a specified Complex, returning
     *     a new Complex.
     *     @param a the real part
     *     @param b the imaginary part
     *     @return <code>Complex</code> the multiplication
     */
    public ComplexF mult(float a, float b) {
	return new ComplexF(a*r - b*i , a*i + b*r);
    }

    /**    Multiply the current by a specified Complex, returning
     *     a new Complex.
     *     @param c the Complex
     *     @return <code>Complex</code> the multiplication
     */
    public ComplexF mult(ComplexF c) {
	return new ComplexF(r*c.r - i*c.i, i*c.r + r*c.i);
    }

    /**    Multiply the current by the conjugate of the
     *     specified Complex retuning a new Complex
     *     @param c the Complex
     *     @return <code>Complex</code> the multiplication
     */
    public ComplexF multConj(ComplexF c) {
	return mult(c.r, -c.i);
    }
    
    /**    Divide the current Complex by a scalar returning a new Complex
     *     @param a the scalar
     *     @return <code>Complex</code> the result
     */
    public ComplexF over(float a) {
	return new ComplexF(r/a , i/a);
    }

    /**    Divide the current by specified Complex returning new Complex
     *     @param a the real part
     *     @param b the imaginary part
     *     @return <code>Complex</code> the result
     */ 
    public ComplexF over(float a, float b) {
	float m = 1.0F/(a*a + b*b);
	return mult(a*m,-b*m);
    }

    /**    Divide the current by specified Complex returning new Complex
     *     @param c the Complex
     *     @return <code>Complex</code> the result
     */ 
    public ComplexF over(ComplexF c) {
	return over(c.r, c.i);
    }

    /**    Divide the specified scalar BY the current Complex returning
     *     a new Complex
     *     @param a the scalar
     *     @return <code>Complex</code> The result
     */
    public ComplexF under(float a) {
	float s = a/modulusSq();
	return new ComplexF(s*r, -s*i);
    }

    /**    Divide the specified Complex  BY the current Complex
     *     @param c the Complex
     *     @return <code>Complex</code> The result
     */
    public ComplexF under(ComplexF c) {
	return c.multConj(this).over(modulusSq());
    }
	
    /**    The Complex conjugate of the current Complex
     *     @return <code>Complex</code> the conjugate
     */
    public ComplexF conj() {
	return new ComplexF(r, -i);
    }

    /**    Form a unit modulus version current Complex retaining the
     *     phase
     *     @return <code>Complex</code> Unit modulus Complex
     */
    public ComplexF unity() {
	float m = modulus();
	if (m != 0.0 )
	    return over(m);
	else
	    return new ComplexF(1.0F);
    }


    /**    Form a scaled Complex from the current Complex with specified
     *     modulus, while retaining phase.
     *     @param mod the specified modulus
     *     @return <code>Complex</code> the scaled Complex.
     */
    public ComplexF scale(float mod) {
    	float m = modulus();
	if (m != 0.0) 
	    return mult(mod/m);
	else
	    return new ComplexF(mod);
    }


    /**     Format the current Complex as a String
     *      @return <code>String</code> formatted Complex
     */
    public String toString() {
	return String.format("[" + ComplexF.fmt + " , " + ComplexF.fmt +"]",
			     this.r,this.i);
    }

    /**     Change the default format, by default set to "%g"
     *      @param fmt the new format String
     */
    public void setFormatString(String fmt) {
    	ComplexF.fmt = fmt;
    }


    /**     Method to add to the current Complex
     *      @param c the Complex to be added
     */
    public void addTo(ComplexF c) {
	r += c.r;
	i += c.i;
    }

    /**     Void method to add to the current Complex
     *      @param r real part to be added
     *      @param i imaginary part of be added
     */
    public void addTo(float r, float i) {
	this.r += r;
	this.i += i;
    }

    /**    Method to mult the current Complex by specified Complex
     *     @param a real part
     *     @param b imaginary part
     */
    public void multBy(float a, float b) {
	float rt = a*r - b*i;
	i = a*i + b*r;
	r = rt;
    }

    /**    Method to mult the current Complex by specified Complex
     *     @param c the specified Complex
     */
    public void multBy(ComplexF c) {
	multBy(c.r,c.i);
    }

    
    /**    Method to mult the current Complex by specified real
     *     @param a the multiplier
     */
    public void multBy(float a) {
	r *= a;
	i *= a;
    }


    /**     Static add method with two parameters
     *      @param a first Complex
     *      @param b second Complex
     *      @return <code>Complex</code> the addition
     */
    public static ComplexF add(ComplexF a, ComplexF b) {
	return a.plus(b);
    }

    /**     Static mult method with two parameters
     *      @param a first Complex
     *      @param b second Complex
     *      @return <code>Complex</code> the multiplication
     */
    public static ComplexF mult(ComplexF a, ComplexF b) {
	return a.mult(b);
    }

}
